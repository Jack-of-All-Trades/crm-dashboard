const webpack = require('webpack'),            
      HtmlWebpackPlugin = require('html-webpack-plugin'),
      _ = require('lodash'),
      helper = require('./helper'),
      MiniCssExtractPlugin = require("mini-css-extract-plugin"),
      envConfig = require('./config.json')
_.forEach(envConfig, function(v, k) {
  envConfig[k] = "'"+v+"'"
})
  
module.exports = {
  devtool: "inline-source-map",
  mode: 'development',
  entry: [
    'react-hot-loader/patch',
    helper.root('src','index')
  ],
  target: "web",
  resolve: {
    extensions: ['.js', '.json', '.ts', '.tsx']
  },
  module: {
    rules: [
      { test: /\.ts?$/, use: "ts-loader", exclude: /node_modules/  },
      { test: /\.tsx?$/, use: "ts-loader", exclude: /node_modules/  },
      { test: /\.js?$/, use: "babel-loader", exclude: /node_modules/ },
      {
        test: /\.scss$/,
        include: [
          helper.root('src/app/base','vendors')
        ],
        use: [
          MiniCssExtractPlugin.loader, 
          {
            loader: 'css-loader',
            options: {
              localIdentName: '[local]',
              modules: true
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /\.scss$/,
        exclude: [
          helper.root('src/app/base','vendors'),
          'font-awesome'
        ],
        use: [
          MiniCssExtractPlugin.loader, 
          {
            loader: 'css-loader',
            options: {
              localIdentName: '[name]_[local]_[hash:8]',
              modules: true
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /\.less$/,
        use: [
          MiniCssExtractPlugin.loader, 
          {
            loader: 'css-loader',
            options: {
              localIdentName: '[name]_[local]_[hash:8]',
              modules: true
            }
          },
          { loader: 'less-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.css/,
        use: [MiniCssExtractPlugin.loader, 'style-loader', 'css-loader']
      },
      {
        // ASSET LOADER
        // Reference: https://github.com/webpack/file-loader
        // Copy png, jpg, jpeg, gif, svg, woff, woff2, ttf, eot files to output
        // Rename the file using the asset hash
        // Pass along the updated reference to your code
        // You can add here any file extension you want to get copied to your output
        test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
        use: 'file-loader'
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            attrs: [':data-src']
          }
        }
      }
    ],
  },
  plugins: [
    // new ExtractTextPlugin({
    //   filename: 'style.css',
    //   allChunks: true
    // }),
    new MiniCssExtractPlugin({
      filename: "bundle_[hash:8].css",
      chunkFilename: "bundle_[hash:8]_[id].css"
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: helper.root('src','index.html'),
      filename: "./index.html"
    }),
    new webpack.DefinePlugin(envConfig)
  ],
  externals: {
    jquery: 'jQuery'
  },
  devServer: {
    inline: true,
    host: "localhost",
    port: 1337,
    historyApiFallback: true,
    disableHostCheck: true
  },
  output: {
    path: helper.root('dist'),
    publicPath: '/',
    filename: "bundle.js",
  },
};
