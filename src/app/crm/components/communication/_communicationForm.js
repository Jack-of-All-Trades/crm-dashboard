import React from 'react'
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';
import { connect } from 'react-redux';
import {Map, List} from 'immutable';

import communicationStyle from './communication.less'

const moment = require('moment'),
      _ = require('lodash')


const CommunicationForm = ({handleValueChange, value, errorText})=>{
  return (
    <div className="container-fluid">
      <form>
        <div className="row">
          <div className="col-sm-12 form-group">
            <TextField 
              floatingLabelText="Remark" 
              multiLine={true}
              rows={3}
              rowsMax ={3}
              fullWidth={true}
              errorText={errorText}
              type="text" 
              onChange={(e,v)=> {
              handleValueChange(v)
            }} value={value} />
          </div>
        </div>
      </form>
    </div>
  )
}

export default CommunicationForm