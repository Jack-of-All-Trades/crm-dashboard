import {Map,List} from 'immutable'
import * as constant from './constants'
import { constants } from 'os';

const initialState = Map({
  formData: Map(),
  conversations: List(),
  dialog: true,
  opportunity: Map({}),
  latestConversation: Map({}),
  customer: Map()
})

export default (state=initialState, action) => {
  let reducer = {}

  reducer[constant.LOADING_CONVSERATION] = ()=> {
    return state.set('conversations', List()).set('conversations',List()).set('dialog',false).set('formData',Map())
  }

  reducer[constant.TOGGLE_DIALOG] = (open) => {
    return state.set('conversations', List()).set('conversations',List()).set('dialog',open).set('formData',Map())
  }

  reducer[constant.LOADED_LATEST_CONVERSATION] = ({conversation, id}) => {
    var latestConversation = state.get('latestConversation')
    return state.set('latestConversation', latestConversation.set(id, conversation))
  }

  reducer[constant.EDIT_CONVERSATION] = ({conversation}) => {
    var formData = state.get('formData')
    formData = formData.set('value',conversation)
    return state.set('formData', formData)
  }

  reducer[constant.LOADED_CONVSERATION] = ({conversations, customer})=> {
    return state
      .set('conversations', List(conversations))
      .set('customer', Map(customer))
      .set('formData',Map({
        identifier: "Customer",
        identifierID: customer.id
      }))
      .set('dialog',true)
  }

  reducer[constant.REFRESH_CONVERSATION] = ({conversations}) => {
    return state
      .set('conversations', List(conversations))
  }

  reducer[constant.UPDATE_COMMUNICATION_FIELD] = ({field, value}) => {
    var formData = state.get('formData')
    formData = formData.set(field, value)
    return state.set('formData',formData)
  }

  reducer[constant.CONVERSATION_SAVED] = ({conversation})=> {
    return state
  }

  reducer['default'] = ()=> {
    return state
  }

  return (reducer[action.type] || reducer['default'])(action)
}
