import * as constants from './constants'
import * as opportunityAction from '../opportunity/action'
import * as customerAction from '../customer/action'
import axios from '../../services/request'
import {Map, List} from 'immutable'

export function loadConversations(modelName, ID) {
  return (dispatch) => {
    dispatch({
      type: constants.LOADING_CONVSERATION
    });
    axios.get('/crm/conversation/'+modelName,{params: {id: ID}})
      .then((data)=>{
        customerAction
          .findCustomer(ID)
          .then((customer)=> {
            if(customer) {
              dispatch({
                type: constants.LOADED_CONVSERATION,
                conversations: data.data,
                customer: customer
              })
            }
          })
      })
  }
}

export function refreshCommunication (modelName, ID) {
  return (dispatch) => {
    dispatch({
      type: constants.REFRESHING_CONVERSATION
    });
    axios.get('/crm/conversation/'+modelName,{params: {id: ID}})
      .then((data)=>{
        dispatch({
          type: constants.REFRESH_CONVERSATION,
          conversations: data.data
        })
      })
  }
}

export function editCommunication(conversation) {
  return (dispatch) => {
    dispatch({
      type: constants.EDIT_CONVERSATION,
      conversation: conversation.value
    });
  }
}

export const toggleDialog = (open) => {
  return {
    type: constants.TOGGLE_DIALOG,
    data: open
  }
}

export const saveRemark = (formData, cb) => {
  return (dispatch) => {
    dispatch({
      type: constants.SAVING_CONVERSATION
    })
    axios.post('/crm/conversation/',formData)
      .then((resp)=>{
        dispatch({
          type: constants.CONVERSATION_SAVED,
          conversation: resp.data
        })
        if(cb) {
          cb(resp.data)
        }
      })
  }
}

export const updateFormField = (field, value) => {
  return {
    type: constants.UPDATE_COMMUNICATION_FIELD,
    field: field,
    value: value
  }
}
