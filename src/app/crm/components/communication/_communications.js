import React from 'react'
import { connect } from 'react-redux';
import { List } from 'immutable';
import IconButton from 'material-ui/IconButton';
import communicationStyle from './communication.less'
import CreateIcon from 'material-ui/svg-icons/content/create'
import { Link } from 'react-router-dom';


import FontIcon from 'material-ui/FontIcon';

const moment = require('moment'),
      status = require('../../share/companyContactStatus.json')

const Communication = ({conversations=List(), auth, editHandler, isEditing}) => {
  if(conversations.count() <= 0)
    return null

  const edit = (userID, conversation) => {
    var addedDate = moment(conversation.addedDate).utcOffset(8)
    var twoWeeksAgo = moment().subtract(1, 'day')
    if(userID == auth.get('id') && _.isEmpty(isEditing) && addedDate.isAfter(twoWeeksAgo)) {
      var onclick = () => {
        editHandler(conversation)
      }
      return <a href="#" onClick={onclick}> <i className="fa fa-pencil" aria-hidden="true"></i></a>
    }
    return null
  }


  const editBefore = (conversation) => {
    if(conversation.auditHistory && conversation.auditHistory.length>1) {
      return <span>, edited</span>
    }    
    return null
  }

  const conversationList = conversations.toArray().map((conversation)=>{
    return (
      <div className="col-sm-12" key={conversation.id}>
        <div className={communicationStyle.communicationContainer}>
          <div>
            <div className={communicationStyle.title}>
              <strong>{conversation.author.profile.displayName}</strong>{" "}
              {edit(conversation.author.id, conversation)}
            </div>
            <div>
              {conversation.value}
            </div>
            <p><small>{moment(conversation.addedDate).utcOffset(8).format('DD/MM/YYYY HH:mm:ss')}{editBefore(conversation)}</small></p>
          </div>
        </div>
      </div>
    )
  })
  return (
    <div className="row">
      {conversationList}
    </div>
  )
}

export default Communication
