import React from 'react'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';
import { connect } from 'react-redux';
import CommunicationForm from './_communicationForm'
import CommunicationList from './_communications'
import { bindActionCreators } from 'redux';
import * as CommunicationActions from './action'
import * as CompanyActions from '../customer/action'
import * as DashBoardActions  from '../dashboard/actions'

function mapStateToProps(state) {
  return {
    conversations: state.crm_communication.get('conversations'),
    company: state.crm_communication.get('customer'),
    formData: state.crm_communication.get('formData'),
    dialog: state.crm_communication.get('dialog'),
    logon: state.Auth.get('userProfile'),
    gridSearch: state.crm_dashboard.get('gridSearch'),
  };
}

@connect(mapStateToProps)
export default class Communication extends React.Component {
  constructor(props) {
    super(props)
    this.dispatch = this.props.dispatch
    this.dashBoardActions = bindActionCreators(DashBoardActions, props.dispatch)
    this.closeDialog = this.closeDialog.bind(this)
    this.saveCommunication = this.saveCommunication.bind(this)
    this.editCommunication = this.editCommunication.bind(this)
    this.handleValueChange = this.handleValueChange.bind(this)
    this.editingID = ""
    this.communicationActions = bindActionCreators(CommunicationActions, props.dispatch)
    this.companyActions = bindActionCreators(CompanyActions, props.dispatch)    
    this.actions = [
      <FlatButton
        label="Cancel"
        primary={false}
        onClick={this.closeDialog}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        onClick={this.saveCommunication}
      />,
    ];
    this.state = {
      value: "",
      editingID: ""
    }
  }

  componentWillMount() {
    var {value} = this.props
    this.setState((prevState)=> {
      return _.extend({}, prevState, {value: value, errorText: ""})
    })
  }

  componentWillReceiveProps(nextProps) {
    if(this.state.value != nextProps.value) {
      this.setState((prevState)=> {
        return _.extend({}, prevState, {value: nextProps.value})
      })
    }
  }
  componentDidMount() {    
    if(!_.isEmpty(this.props.match.params.companyID)) {
      this.communicationActions.loadConversations('Company',this.props.match.params.companyID)
    }
  }

  closeDialog() {
    let {gridSearch} = this.props
    this.dashBoardActions.loadList(gridSearch)
    this.communicationActions.toggleDialog(false)
    this.props.history.push('/crm')
  }

  saveCommunication() {
    this.setState((prevState)=> {
      return _.extend({}, prevState, {errorText: ""})
    })

    if(_.isEmpty(this.state.value)) {
      this.setState((prevState)=> {
        return _.extend({}, prevState, {errorText: "This is a required field"})
      })
      return;
    }

    var formData = {
      identifier:'Company',
      identifierID: this.props.match.params.companyID,
      value: this.state.value,
      id: this.state.editingID
    }
    var editing = false
    if(this.state.editingID) {
      editing = true
    }
    var self = this
    this.communicationActions.saveRemark(formData, function(data) {
      self.communicationActions.refreshCommunication('Company',self.props.match.params.companyID)
      if(!editing)
        self.closeDialog()
    })
    this.setState((prevState)=> {
      return _.extend({}, prevState, {value: "",editingID: ""})
    })
  }

  editCommunication(conversation) {
    this.setState((prevState)=> {
      return _.extend({}, prevState, {value: conversation.value, editingID: conversation.id})
    })
  }

  handleValueChange(value) {
    this.setState((prevState)=> {
      return _.extend({}, prevState, {value: value})
    })
  }

  render() {
    const {company, conversations, formData, dialog, logon, refreshGrid} = this.props
    var title = company.get('name')+"'s Conversation"
    if(this.state.editingID!="") {
      title = "Editing "+title
    }
    return (
      <Dialog
        title={title}
        actions={this.actions}
        model={true}
        autoScrollBodyContent={true}
        open={dialog}
        autoLockScrolling={true}
        onRequestClose={this.closeDialog} >
        <CommunicationForm value={this.state.value} handleValueChange={this.handleValueChange} errorText={this.state.errorText}/>
        <CommunicationList isEditing={this.state.editingID} editHandler={this.editCommunication} conversations={conversations} auth={logon}/>
      </Dialog>
    )
  }
}
