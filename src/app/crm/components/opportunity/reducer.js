import * as OpportunityConstant from './constants'
import {Map} from 'immutable'
var moment = require('moment')

const initialState = Map({
  data: Map({
    stage: "new"
  }),
  loaded: false,
  dialog: true,
  isNew: true,
  saving: false,
  loadedOwner: Map(),
  loadedCompany: Map(),
  customeOopportunities: []
})

export default (state=initialState, action) => {
  let reducer = {}
  reducer[OpportunityConstant.LOADING_OPPORTUNITY] = ()=> {
    return state.set('loaded', false)
  }
  reducer[OpportunityConstant.RESET_OPPORTUNITY_DATA] = ()=> {
    return state.set('data',Map({
      stage: "new"
    })).set('loaded', true).set('isNew',true)
  }
  reducer[OpportunityConstant.LOADED_OPPORTUNITY] = ({data})=> {
    data = data
      .set('campaignEndDate',moment(data.get('campaignEndDate')).toDate())
      .set('campaignStartDate',moment(data.get('campaignStartDate')).toDate())
    return state.set('data',data).set('loaded', true).set('isNew',false).set('dialog', true)
  }
  reducer[OpportunityConstant.SAVED_OPPORTUNITY] = ()=> {
    return state.set('saving',false)
  }
  reducer[OpportunityConstant.SAVING_OPPORTUNITY] = ()=> {
    return state.set('saving',true)
  }
  reducer[OpportunityConstant.LOAD_ALL_OPPORTUNITY] = ()=> {
    return state.set('customeOopportunities',[])
  }
  reducer[OpportunityConstant.LOADED_ALL_OPPORTUNITY] = ({data}) => {
    return state.set("customeOopportunities", data).set('dialog', true)
  }
  reducer[OpportunityConstant.UPDATE_OPPORTUNITY_FIELD] = ({field,data})=> {
    var d = state.get('data')
    d=d.set(field,data)
    return state.set('data',d)
  }
  reducer[OpportunityConstant.TOGGLE_OPPORTUNITY_DIALOG] = ({data}) => {
    return state.set('dialog',data)
  }

  reducer[OpportunityConstant.LOADING_COMPANY] = () => {
    return state.set('loadedCompany', Map())
  }

  reducer[OpportunityConstant.LOADED_COMPANY] = ({company}) => {
    var d = state.get('data')
    d=d.set('companyID',company.id)
    return state.set('loadedCompany', Map(company)).set('data',d)
  }

  reducer[OpportunityConstant.LOADING_OWNER] = ()=> {
    return state;
  }

  reducer[OpportunityConstant.LOADED_OWNER] = ({data})=> {
    var owner = state.get('loadedOwner')
    owner = owner.set(data._id,data)
    return state.set('loadedOwner', owner)
  }

  reducer['default'] = ()=> {
    return state
  }
  return (reducer[action.type] || reducer['default'])(action)
}
