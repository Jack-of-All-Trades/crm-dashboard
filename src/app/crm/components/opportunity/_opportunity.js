import React from 'react'


var moment = require('moment')
var numeral = require('numeral')

const Opportunities = ({opportunities}) => {
  if(!opportunities)
    return null
  var list = opportunities.map((value)=>{
    return (<dl className="row" key={value.id}>
      <dt className="col-sm-3">Campaign Date</dt>
      <dd className="col-sm-9">{moment(value.campaignStartDate).format('DD/MM/YYYY')}</dd>
      <dt className="col-sm-3">Package Price</dt>
      <dd className="col-sm-9">{numeral(value.packagePrice).format('$0,0.00')}</dd>
      <dt className="col-sm-3">Chance</dt>
      <dd className="col-sm-9">{value.chance.value}</dd>
      <dt className="col-sm-3">Remark</dt>
      <dd className="col-sm-9">{value.remark}</dd>
    </dl>)
  })
  return (
    <div className="container-fluid">
      {list}
    </div>
  )
}

export default Opportunities
