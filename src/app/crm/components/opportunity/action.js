import * as OpportunityConstant from './constants'
import * as CompanyActions from '../customer/action'
import axios from '../../services/request'
import {Map} from 'immutable'

var loadedOpportunity = new Map()
var latestOpportunity = new Map()

export const getLatestOpportunityForCustomer = (customerID) => {
  if(!latestOpportunity.has(customerID)) {
    var promise = axios.get('/crm/opportunity/by-customer/'+customerID)
      .then((data)=> {
        return data.data
      })
    latestOpportunity = latestOpportunity.set(customerID, promise)
  }
  return latestOpportunity.get(customerID)
}

export const getCustomerOpportunities = (customerID) => {
  return (dispatch) => {
    dispatch({
      type: OpportunityConstant.LOAD_ALL_OPPORTUNITY
    })
    axios.get('/crm/opportunity/by-company/'+customerID)
      .then((data)=> {
        dispatch({
          type: OpportunityConstant.LOADED_ALL_OPPORTUNITY,
          data: data.data
        })
      })
  }


  if(!latestOpportunity.has(customerID)) {
    var promise = axios.get('/crm/opportunity/by-customer/'+customerID)
      .then((data)=> {
        return data.data
      })
  }
  return latestOpportunity.get(customerID)
}

export const updateData = (field,data) => {
  return {
    type: OpportunityConstant.UPDATE_OPPORTUNITY_FIELD,
    field: field,
    data: data
  }
}

export const update = (oppID, data) => {
  dispatch({
    type: OpportunityConstant.SAVING_OPPORTUNITY
  })
  axios.post('/crm/opportunity/',data)
  .then((resp)=>{
    loadData(dispatch, resp.data)
    dispatch({
      type: OpportunityConstant.SAVED_OPPORTUNITY
    })
  })
}

export const setCompany = (data)=> {
  return (dispatch) => {
    dispatch({
      type: OpportunityConstant.LOADED_COMPANY,
      company: data
    })
  }
}

export const getCompany = (id)=>{
  return (dispatch) => {
    dispatch({
      type: OpportunityConstant.LOADING_COMPANY
    })
    CompanyActions.findCustomer(id, false)
      .then(function(company) {
        dispatch({
          type: OpportunityConstant.LOADED_COMPANY,
          company: company
        })
      })
  }
}

export const resetData = () => {
  return {
    type: OpportunityConstant.RESET_OPPORTUNITY_DATA
  }
}

export const loadOpportunity = (id)=>{
  return (dispatch) => {
    dispatch({
      type: OpportunityConstant.LOADING_OPPORTUNITY
    });
    axios.get('/crm/opportunity/'+id)
      .then((resp)=>{
        loadData(dispatch, resp.data)
      })
  }
}

export const toggleDialog = (open) => {
  return {
    type: OpportunityConstant.TOGGLE_OPPORTUNITY_DIALOG,
    data: open
  }
}

var loadedOwnerID = []

export const getOwner = (id) => {
  return (dispatch) => {
    if(loadedOwnerID.indexOf(id)<0) {
      dispatch({
        type: OpportunityConstant.LOADING_OWNER
      })
      loadedOwnerID.push(id)
      axios.get('/user/'+id)
        .then((resp)=>{
          dispatch({
            type: OpportunityConstant.LOADED_OWNER,
            data:resp.data
          })
        })
    }

  }
}

const loadData = (dispatch, data)=>{
  dispatch({
    type: OpportunityConstant.LOADED_OPPORTUNITY,
    data: Map(data)
  })
}

export const save = (data,cb) => {
  return (dispatch) => {
    dispatch({
      type: OpportunityConstant.SAVING_OPPORTUNITY
    })
    var request
    var url = '/crm/opportunity/'
    if(data.get("id", false)) {
      url += data.get("id", false)
      delete(data.delete("id"))
      request = axios.patch(url,data)
    } else {
      request = axios.post(url,data)
    }
    request
      .then((resp)=> {
        loadData(dispatch, resp.data)
        dispatch({
          type: OpportunityConstant.SAVED_OPPORTUNITY
        })
        if(cb)
          cb(resp.data)
      })
  }
}
