import React from 'react'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import OpportunityForm from './_form'
import * as OpportunityActions from './action'
import {listCustomer} from '../customer/action'
import * as DashboardActions from '../dashboard/actions'
import DatePicker from 'material-ui/DatePicker';
import Opportunities from './_opportunity';

const querystring = require('querystring');

function mapStateToProps(state) {
  return {
    opportunity: state.crm_opportunity.get('data'),
    dialogOpen: state.crm_opportunity.get('dialog'),
    customers: state.crm_customer.get('allCustomers'),
    isSaving: state.crm_opportunity.get('saving'),
    gridSearch: state.crm_dashboard.get('gridSearch'),
    referenceConfig: state.crm_dashboard.get('referenceConfig'),
    company: state.crm_opportunity.get('loadedCompany'),
    opportunities: state.crm_opportunity.get('customeOopportunities')
  };
}

@connect(mapStateToProps)
export default class Opportunity extends React.Component {
  constructor(props) {
    super(props)
    this.boundActionCreators = bindActionCreators(OpportunityActions, this.props.dispatch)
    this.dashboardActions = bindActionCreators(DashboardActions, props.dispatch)
    this.dispatch = this.props.dispatch
    this.closeDialog = this.closeDialog.bind(this)
    this.showFUPField = this.showFUPField.bind(this)
    this.saveCustomer = this.saveCustomer.bind(this)
    this.forceOpen = false
    this.saved = false
    this.state = {
      errorFields: {}
    }
    this.actions = [
      <FlatButton
        label="Cancel"
        primary={false}
        onClick={this.closeDialog}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        onClick={this.saveCustomer}
      />,
    ];
  }

  componentWillMount(){
    this.boundActionCreators.resetData()
    if(!_.isEmpty(this.props.match) && !_.isEmpty(this.props.match.params.customerID)) {
      this.boundActionCreators.getCustomerOpportunities(this.props.match.params.customerID)
      this.boundActionCreators.getCompany(this.props.match.params.customerID)
    }
    if(!_.isEmpty(this.props.match) && !_.isEmpty(this.props.match.params.id)) {
      this.boundActionCreators.loadOpportunity(this.props.match.params.id)
    } else {
      if(this.props.needFollowUpDate) {
        this.forceOpen = true
        if(this.props.sendCompany) {
          this.boundActionCreators.setCompany(this.props.sendCompany)
          this.boundActionCreators.getCustomerOpportunities(this.props.sendCompany.id)
        }
      }
    }
  }

  closeDialog() {
    this.boundActionCreators.toggleDialog(false)
    this.dashboardActions.loadList(this.props.gridSearch)
    if(this.props.cancelSave) 
      this.props.cancelSave()
    this.props.history.push('/crm')
  }

  saveCustomer() {
    var self = this
    this.setState((prevState)=> {
      return _.extend({}, prevState, {errorFields: {}})
    })
    var errorFields = {}
    var {opportunity} = this.props

    if(opportunity.get('chance') == "won" || opportunity.get('chance') == "lost") {
      if(_.isEmpty(opportunity.get('remark'))) {
        errorFields['remark'] = "Cannot be empty"
      }
    }
    if(_.isEmpty(opportunity.get('campaignStartDate',"").toString())) {
      errorFields['campaignStartDate'] = "Cannot be empty"
    }
    var price = opportunity.get('packagePrice')
    if(_.isEmpty(price) && !_.isNumber(price)) {
      errorFields['packagePrice'] = "Cannot be empty"
    }
    if(!opportunity.get('chanceID') || opportunity.get('chanceID') <=0) {
      errorFields['chance'] = "Cannot be empty"
    }
    if(this.props.needFollowUpDate) {
      errorFields = this.props.validateField(errorFields)
    }
    if(!_.isEmpty(errorFields)) {
      return this.setState((prevState)=> {
        return _.extend({}, prevState, {errorFields: errorFields})
      })
    } else {
      if(this.props.afterSave != null) {
        var saveData = this.props.opportunity
        this.boundActionCreators.save(this.props.opportunity, this.props.afterSave)
      } else {
        this.boundActionCreators.save(this.props.opportunity, function() {
          self.dashboardActions.loadList(self.props.gridSearch)
        })

      }
      this.saved = true
      this.closeDialog(false)
    }
  }

  showFUPField() {
    if(this.props.needFollowUpDate) {
      return (
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-12">
            <DatePicker
                hintText="Follow Up Date"
                formatDate={function(date) {
                  return moment(date).format('DD/MM/YYYY')
                }}
                autoOk={true}
                minDate={new Date()}
                onChange={(i,v)=>{
                  updateChange(v)
                }}
              />
            </div>
          </div>
        </div>
      )
    }
    return null
  }

  render() {
    const {dialogOpen, opportunity, company, referenceConfig, opportunities} = this.props
    var title = "Opportunity"
    if(company) {
      title += " for "+company.get('name')
    }
    var chanceRef = []
    if(referenceConfig.has('chance'))
      chanceRef = referenceConfig.get('chance')
    return (
      <Dialog
        title={title}
        actions={this.actions}
        open={dialogOpen}
        autoScrollBodyContent={true}
        autoLockScrolling={true}
        model={this.forceOpen} >
        <OpportunityForm fupField={this.props.needFollowUpDate} opportunity={opportunity} chanceRef={chanceRef} errorFields={this.state.errorFields} {...this.boundActionCreators} />
        <hr />
        <Opportunities opportunities={opportunities} />
      </Dialog>
    )
  }
}
