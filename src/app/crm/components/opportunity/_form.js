import React from 'react'
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import AutoComplete from 'material-ui/AutoComplete';
import DatePicker from 'material-ui/DatePicker';
import { connect } from 'react-redux';
import MenuItem from 'material-ui/MenuItem';
import {Map} from 'immutable';
import SuperSelectField from 'material-ui-superselectfield'
const moment = require('moment'),
      _ = require('lodash'),
      chance = require('../../share/chance.json'),
      stage = require('../../share/stage.json')


export default ({opportunity=Map(), updateData, company, errorFields, chanceRef, fupField}) => {  
  const Chance = ()=> {
    return chanceRef.map(function(k) {
      return (
        <MenuItem value={k.id} key={k.id} primaryText={k.value} />
      )
    })
  }
  const showExtraFields = ()=>{
    if(fupField) {
      return fupField()
    }
    return null
  }
  
  return (
    <div className="container-fluid">
      <form>
        {showExtraFields()}
        <div className="row">
          <div className="col-sm-4">
            <DatePicker 
              floatingLabelText="Campaign Date"
              autoOk={true}
              fullWidth={true}
              errorText={errorFields.campaignStartDate}
              onChange={(i,v)=> {
              updateData('campaignStartDate',v)
            }} value={opportunity.get('campaignStartDate')} />
          </div>
          <div className="col-sm-4">
            <TextField
              fullWidth={true}
              floatingLabelText={"Package Price"}
              errorText={errorFields.packagePrice}
              defaultValue={opportunity.get('packagePrice')}
              value={opportunity.get('packagePrice')}
              onChange={(e,v)=> {
                updateData('packagePrice',v)
              }}
            />
          </div>
          <div className="col-sm-4">
            <SelectField floatingLabelText="Chance" fullWidth={true} errorText={errorFields.chance} onChange={(e,i,v)=> {
              updateData('chanceID',v)
            }} value={opportunity.get('chanceID')}>
              {Chance()}
            </SelectField>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <TextField
              fullWidth={true}
              floatingLabelText={"Remark"}
              multiLine={true}
              rows={2}
              errorText={errorFields.remark}
              defaultValue={opportunity.get('remark')}
              value={opportunity.get('remark')}
              onChange={(e,v)=> {
                updateData('remark',v)
              }}
            />
          </div>
        </div>
      </form>
    </div>
  )
}