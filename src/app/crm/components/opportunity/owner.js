import React from 'react'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import * as OpportunityActions from './action'

function mapStateToProps(state) {  
  return { 
    owners: state.crm_opportunity.get('loadedOwner')
  };
}

@connect(mapStateToProps)
export default class Owner extends React.Component {
  constructor(props) {
    super(props)    
    this.boundActionCreators = bindActionCreators(OpportunityActions, this.props.dispatch)
    this.dispatch = this.props.dispatch
  }

  componentWillMount(){
    this.boundActionCreators.resetData()
  }

  componentDidMount() {
    const { owners, ownerID } = this.props
    if(!owners.has(ownerID)) {
      this.boundActionCreators.getOwner(ownerID)
    }
  }

  render() {
    const { owners, ownerID } = this.props
    if(owners.has(ownerID)) {
      return (
        <div>
          {owners.get(ownerID).username}
        </div>
      )
    }
    return null
  }
}