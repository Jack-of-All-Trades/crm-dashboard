import fetch from 'isomorphic-fetch'

var user = null;

export default () => {
  if(user !== null) {
    return new Promise((resolve,reject)=>{
      resolve(user)
    })
  }
  return fetch('/auth/me')
    .then((data)=>{
      user = Map(data.json())
      return user
    })
}