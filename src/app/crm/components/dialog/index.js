import React from 'react'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';


export default ()=> {
  const actions = [
    <FlatButton
      label="Cancel"
      primary={true}
      onClick={this.handleClose}
    />,
    <FlatButton
      label="Submit"
      primary={true}
      keyboardFocused={true}
      onClick={this.handleClose}
    />,
  ];

  return (
    <div>
      <h1>Events</h1>
      <FloatingActionButton style={style}>
        <ContentAdd />
      </FloatingActionButton>
      <Dialog
        title="Add Event"
        actions={actions}
        model={false}
        open={this.state.open}
        onRequestClose={this.handClose}
      >
        <TextField
          hintText="Hint Text"
          floatingLabelText="Floating Label Text"
        />
      </Dialog>
    </div>
  )
}