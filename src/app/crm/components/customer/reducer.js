import {UPDATE_CONTACT_FIELD, LOADING_CUSTOMER, NEW_CUSTOMERS, LOADED_CUSTOMER, UPDATE_CUSTOMER_FIELD, TOGGLE_CUSTOMER_DIALOG, LISTED_CUSTOMERS, ADD_CONTACT} from './constants'
import * as Constants from './constants'
import {Map,List} from 'immutable'

const initialState = Map({
  data: Map({}),
  customerLoaded: false,
  customerDialog: true,  
  isNewCustomer: true,
  loadedCustomer: Map({}),
  allCustomers: List(),
  overdueFUPDate: [],
  updatingOverDate: false,
  needOpportunity:[]
})

export default (state=initialState, action) => {
  let reducer = {}

  reducer[LOADING_CUSTOMER] = ()=> {
    return state.set('customerLoaded', false).set('data',Map())
  }

  reducer[LISTED_CUSTOMERS] = ({data})=> {
    return state.set('allCustomers', List(data))
  }

  reducer[UPDATE_CONTACT_FIELD] = ({field, value, index}) => {
    var data = state.get('data')
    var contacts = data.get('contacts')
    var contact = contacts.get(index)
    contact = contact.set(field,value)
    contacts = contacts.set(index, contact)
    data = data.set('contacts', contacts)
    return state.set('data',data)
  }

  reducer[Constants.GETTING_OPPORTUNITY] = ()=> {
    return state.set('needOpportunity',[])
  }

  reducer[Constants.GOT_OPPORTUNITY] = ({data})=> {
    return state.set('needOpportunity',data)
  }

  reducer[Constants.REMOVE_OPPORTUNITY] = () => {
    return state.set('needOpportunity',[])
  }

  reducer[Constants.GETTING_OVERDUE] = () => {
    return state.set('overdueFUPDate', [])
  }

  reducer[Constants.UPDATE_OVERDUE] = ({data}) => {
    var overdue = state.get('overdueFUPDate')
    var newOverDue = []
    overdue.map(function(due) {
      if(due.id == data.id) {
        newOverDue.push(data)
      } else {
        newOverDue.push(due)
      }
    })
    return state.set('overdueFUPDate', newOverDue)
  }

  reducer[Constants.DONE_GETTING_OVERDUE] = ({data}) => {
    return state.set('overdueFUPDate', data)
  }

  reducer[ADD_CONTACT] = () => {
    var data = state.get('data')
    var contacts = List()
    if(data.get('contacts')) {
      contacts = data.get('contacts')
    }
    contacts = contacts.push(Map({statusID:20}))
    data = data.set('contacts',contacts)
    return state.set('data', data)
  }

  reducer[NEW_CUSTOMERS] = ()=> {
    var newContact = new Map({
      status:"active"
    })
    var contact = List()
    contact.push(newContact)
    return state
      .set('isNewCustomer', true)
      .set('data',Map({
        'operationID':1,
        contacts: contact
      }))
      .set('customerDialog', true)      
  }

  reducer[LOADED_CUSTOMER] = ({data})=> {
    if(data.get('contacts').size<=0) {
      var newContact = new Map({
        status:"active"
      })
      var contacts = List()
      contacts = contacts.push(newContact)      
      data = data.set('contacts',contacts)
    }

    return state
      .set('data',data)
      .set('customerLoaded', true)
      .set('customerDialog', true)
      .set('isNewCustomer',false)
      .set('loadedCustomer',state.get('loadedCustomer').set(data.get('_id'),data))
  }

  reducer[UPDATE_CUSTOMER_FIELD] = ({field,data})=> {
    var d = state.get('data')
      d=d.set(field,data)
      return state.set('data',d)
  }

  reducer[TOGGLE_CUSTOMER_DIALOG] = ({open}) => {
    return state.set('customerDialog',open)
  }

  reducer['default'] = ()=> {
    return state
  }

  return (reducer[action.type] || reducer['default'])(action)
}
