import React from 'react'
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import Subheader from 'material-ui/Subheader'
import MenuItem from 'material-ui/MenuItem';
import { connect } from 'react-redux';
import Paper from 'material-ui/Paper';
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';
const moment = require('moment'),
      status = require('../../share/companyContactStatus.json')


const SortableItem = SortableElement(({contactIndex, refList, contact, contactFieldChangeHandle}) => {
  const Status = ()=> {
    if(refList.has('contactStatus')) {
      return refList.get('contactStatus').map(function(v) {
        return (
          <MenuItem value={v.id} key={v.id} primaryText={v.value} />
        )
      })
    }
    return null
  }
  return (
    <Paper zDepth={1} rounded={true}>
      <div className="container">
        <div className="row">
          <div className="col-sm-6 form-group">
            <TextField
              floatingLabelText="Name"
              type="text"
              fullWidth={true}
              value={contact.get('name')}
              onChange={(e,v)=> {
                contactFieldChangeHandle(contactIndex, 'name', v)
              }}
            />
          </div>
          <div className="col-sm-6 form-group">
            <TextField
              floatingLabelText="Title"
              type="text"
              fullWidth={true}
              value={contact.get('title')}
              onChange={(e,v)=> {
                contactFieldChangeHandle(contactIndex, 'title', v)
              }}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6 form-group">
            <TextField
              floatingLabelText="Email"
              type="email"
              fullWidth={true}
              value={contact.get('email')}
              onChange={(e,v)=> {
                contactFieldChangeHandle(contactIndex, 'email', v)
              }}
            />
          </div>
          <div className="col-sm-6 form-group">
            <TextField
              floatingLabelText="Contact"
              type="text"
              fullWidth={true}
              value={contact.get('contact')}
              onChange={(e,v)=> {                
                contactFieldChangeHandle(contactIndex, 'contact', v)
              }}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6 form-group">
            <SelectField floatingLabelText="Status" fullWidth={true}  onChange={(e,i,v)=> {
              contactFieldChangeHandle(contactIndex, 'statusID',v)
            }} value={contact.get('statusID')}>
              {Status()}
            </SelectField>
          </div>
        </div>
      </div>

    </Paper>
  )
});

const SortableList = SortableContainer(({contacts=List(), refList, contactFieldChangeHandle})=>{  
  return (
    <div>
      <Subheader>Contacts</Subheader>
      {contacts.map((value, index) => (
        <SortableItem key={`item-${value.id}`} refList={refList} index={index} contactIndex={index} contact={value} contactFieldChangeHandle={contactFieldChangeHandle} />
      ))}
    </div>
  )
})

export default SortableList
