import React from 'react'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';

import { connect } from 'react-redux';
import CustomerForm from './_customerForm'
import { bindActionCreators } from 'redux';
import * as CustomerActions from './action'
import * as DashboardActions from '../dashboard/actions'



function mapStateToProps(state) {
  return {
    data: state.crm_customer.get('data'),
    customerDialog: state.crm_customer.get('customerDialog'),
    gridSearch: state.crm_dashboard.get('gridSearch'),
    referenceConfig: state.crm_dashboard.get('referenceConfig'),
    isNewCustomer: state.crm_customer.get('isNewCustomer'),
    allUsers: state.user.get("allUsers"),
    logonUser: state.Auth.get('userProfile')
  };
}

@connect(mapStateToProps)
export default class Customer extends React.Component {
  constructor(props) {
    super(props)
    this.dispatch = this.props.dispatch
    this.fieldChange = this.fieldChange.bind(this)
    this.closeDialog = this.closeDialog.bind(this)
    this.saveCustomer = this.saveCustomer.bind(this)
    this.addContact = this.addContact.bind(this)
    this.contactFieldChange = this.contactFieldChange.bind(this)
    this.customerActions = bindActionCreators(CustomerActions, props.dispatch)
    this.dashboardActions = bindActionCreators(DashboardActions, props.dispatch)
    this.isNewCustomer = this.props.isNewCustomer
    this.actions = [
      <FlatButton
        label="Cancel"
        primary={false}
        onClick={this.closeDialog}
      />,
      <FlatButton
        label="Add a contact"
        primary={false}
        onClick={this.addContact}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        onClick={this.saveCustomer}
      />,
    ];
  }

  componentDidMount() {
    if(!_.isEmpty(this.props.match.params.customerID)) {
      this.customerActions.loadCustomer(this.props.match.params.customerID)
    } else {
      this.customerActions.newCustomer()
      this.customerActions.addContact()
    }
  }

  addContact() {
    this.customerActions.addContact()
  }

  fieldChange(field,value) {
    this.customerActions.updateCustomerData(field,value)
  }

  contactFieldChange(index, field,value) {
    this.customerActions.updateContactField(index, field,value)
  }

  closeDialog() {
    this.customerActions.toggleCustomerDialog(false)
    this.props.history.push('/crm')
    this.dashboardActions.loadList(this.props.gridSearch)
    //this.customerActions.toggleCustomerDialog(false)
  }

  saveCustomer() {
    var self = this
    setTimeout(function() {
      self.customerActions.saveCustomer(self.props.data, self.props.data.get('contacts'), ()=> {
        self.closeDialog()
      })
    }, 400)

  }

  render() {
    const {customerDialog, data,referenceConfig, logonUser, allUsers} = this.props    
    return (
      <Dialog
        title="Company"
        actions={this.actions}
        model={true}
        autoScrollBodyContent={true}
        open={customerDialog}
        autoLockScrolling={true}
        onRequestClose={this.closeDialog} >
        <CustomerForm logonUser={logonUser} allUsers={allUsers} customer={data} refList={referenceConfig} contactFieldChangeHandle={this.contactFieldChange} handleValueChange={this.fieldChange} customerContacts={data.get('contacts')} />
      </Dialog>
    )
  }
}
