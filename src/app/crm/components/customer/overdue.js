import React from 'react'
import { connect } from 'react-redux';
import {FlatButton, Dialog, MenuItem, DatePicker, SelectField} from 'material-ui'
import {Map} from 'immutable';

import * as Constants from './constants'

import { bindActionCreators } from 'redux';
import * as CustomerActions from './action'
import * as DashboardActions from '../dashboard/actions'
import overdueStyle from './overdue.less'

var moment = require('moment')


const OverDueForm = ({data,updateChange}) => {
  return (
    <div className={overdueStyle.overdueContainer+" row align-items-center"}>
      <div className="col-5">
        {data.name}
      </div>
      <div className="col-7">
        <DatePicker
          hintText="Controlled Date Input"
          formatDate={function(date) {
            return moment(date).format('DD/MM/YYYY')
          }}
          defaultDate={moment(data.followUpDate).toDate()}
          value={moment(data.followUpDate).toDate()}
          autoOk={true}
          minDate={new Date()}
          onChange={(i,v)=>{
            data['followUpDate'] = v
            updateChange(data)
          }}
        />
      </div>
    </div>
  )
}


function mapStateToProps(state) {
  return {
    overdueFUPDate: state.crm_customer.get('overdueFUPDate'),
    gridSearch: state.crm_dashboard.get('gridSearch')
  };
}

@connect(mapStateToProps)
export default class OverDue extends React.Component  {
  constructor(props) {
    super(props)
    this.customerActions = bindActionCreators(CustomerActions, props.dispatch)
    this.dashBoardActions = bindActionCreators(DashboardActions, props.dispatch)
    this.saveOverDue = this.saveOverDue.bind(this)
    this.actions = [
      <FlatButton
        label="Save"
        primary={true}
        onClick={this.saveOverDue}
      />
    ];
    this.updated = false

  }

  saveOverDue() {
    var {overdueFUPDate, refreshGrid} = this.props
    var self = this
    this.customerActions.updateFUPDate(overdueFUPDate, ()=> {
      self.customerActions.getOverDue()
      refreshGrid()
    })
  }

  componentDidMount() {
    this.customerActions.getOverDue()
  }

  render() {
    var {overdueFUPDate} = this.props
    var showDialog = false
    var form = []
    if(overdueFUPDate.length>0) {
      if(!this.updated) {
        var newOverDue = []
        overdueFUPDate.map(function(data) {
          data.followUpDate = new Date()
          newOverDue.push(data)
        })
        this.updated = true
        overdueFUPDate = newOverDue
      }
      showDialog = true
      var self = this
      form = overdueFUPDate.map(function(data) {
        return (<OverDueForm key={data.id} data={data} updateChange={self.customerActions.updateOverDue} />)
      })
      return (
        <Dialog
          title="Overdue Follow Ups!"
          actions={this.actions}
          model={true}
          autoScrollBodyContent={true}
          open={true}
          autoLockScrolling={true} >
          {form}
        </Dialog>
      )
    } else {
      return null
    }

  }
}
