import {UPDATE_CONTACT_FIELD, ADD_CONTACT, LOADING_CUSTOMER, LOADED_CUSTOMER, NEW_CUSTOMERS, UPDATE_CUSTOMER_FIELD, TOGGLE_CUSTOMER_DIALOG, SAVING_CUSTOMER, LISTED_CUSTOMERS} from './constants'
import * as Constants from './constants'
import fetch from 'isomorphic-fetch'
import axios from '../../services/request'
import {Map, List} from 'immutable'
import parallelLimit from 'async/parallelLimit';

const type = require('../../share/type.json')

export async function findCustomer(customerID, needContacts=true) {
  try {
    var customerResp = await axios.get('/crm/company/'+customerID),
        customerModel = customerResp.data
    customerModel.contacts = List()
    if(needContacts) {
      var contactsResp = await axios.get('/crm/company/'+customerID+'/contacts'),
          contactsData = contactsResp.data,
          contactList = List()
      _.forEach(contactsData, function(contact) {
        contactList = contactList.push(Map(contact))
      })
      customerModel.contacts = contactList
    }
    return customerModel
  } catch(e) {
    return null
  }
}

export function getNeedOpportunity() {
  return (dispatch) => {
    dispatch({
      type: Constants.GETTING_OPPORTUNITY
    });
    axios.get('/crm/company/needOpportunity')
      .then((overdue)=> overdue.data)
      .then((overdue) => {
        dispatch({
          type: Constants.GOT_OPPORTUNITY,
          data: overdue
        })
      })

  }
}

export function removeNeedOpportunity() {
  return (dispatch) => {
    dispatch({
      type: Constants.REMOVE_OPPORTUNITY
    });
  }
}

export function getOverDue() {
  return (dispatch) => {
    dispatch({
      type: Constants.GETTING_OVERDUE
    });
    axios.get('/crm/company/overdue')
      .then((overdue) => {
        dispatch({
          type: Constants.DONE_GETTING_OVERDUE,
          data: overdue.data
        })
      })
  }
}

export function loadCustomer(customerID) {
  return (dispatch) => {
    dispatch({
      type: LOADING_CUSTOMER
    });
    findCustomer(customerID)
      .then(function(customer) {
        if(customer) {
          dispatch({
            type: LOADED_CUSTOMER,
            data: Map(customer)
          })
        }
      })
  }
}

export const addContact = ()=> {
  return {
    type: ADD_CONTACT
  }
}

export const updateContactField = (index, field, value) => {  
  return {
    type: UPDATE_CONTACT_FIELD,
    index: index,
    field: field,
    value: value
  }
}


export const listCustomer = ()=>{
  return (dispatch) => {
    dispatch({
      type: LOADING_CUSTOMER
    });
    axios('/crm/company')
      .then((data)=>{
        return data.data
      })
      .then((data)=> {
        dispatch({
          type: LISTED_CUSTOMERS,
          data: data
        })
      })
  }
}

export const newCustomer = () => {
  return {
    type: NEW_CUSTOMERS
  }
}

export const searchCustomer = (name)=>{
  return (dispatch) => {
    dispatch({
      type: LOADING_CUSTOMER
    });
    fetch(config.apiURL+'/crm/company/'+customerID,{ credentials: 'include'})
      .then((data)=>{
        return Map(data.json())
      })
      .then((data)=> {
        dispatch({
          type: LOADED_CUSTOMER,
          data: data
        })
      })
  }
}

export const toggleCustomerDialog = (open) => {
  return {
    type: TOGGLE_CUSTOMER_DIALOG,
    open: open
  }
}

export const saveContacts = (dispatch, company, contacts, cb) => {
  axios.post('/crm/company/'+company.id+'/contacts/',contacts)
  .then((resp) => {
    var contacts = resp.data
    var contactList = List()
    _.forEach(contacts, function(contact) {
      contactList = contactList.push(Map(contact))
    })
    company.contacts = contactList
    // dispatch({
    //   type: LOADED_CUSTOMER,
    //   data: Map(company),
    //   contacts: contactList
    // })
    //dispatch(listCustomer())
    cb()
  })
}

export const updateFUPDate = (fupDates, cb) => {
  return (dispatch) => {
    var saveArray = []
    fupDates.map(function(data) {
      saveArray.push(function(cb) {
        axios.post('/crm/company/',data)
          .then((data)=>{
            cb(null, data.data)
          })
          .catch((e)=>cb(e))
      })
    })
    parallelLimit(saveArray, 5, (errors, datas)=> {
      cb(datas)
    })
  }
}

export const saveCustomer = (data, contacts, cb) => {
  return (dispatch) => {
    dispatch({
      type: SAVING_CUSTOMER
    })
    var url = '/crm/company/'
    axios.post(url,data)
      .then((resp)=>{
        if(!_.isEmpty(contacts))
          saveContacts(dispatch, resp.data, contacts, cb)
        else {
          cb(resp.data)
        }
      })
  }
}

export const updateCustomerData = (field,data) => {
  return {
    type: UPDATE_CUSTOMER_FIELD,
    field: field,
    data: data
  }
}

export const updateCommunication = (customerID) => {
  return axios.post('/crm/company/latestConversation/'+customerID)
}

export const updateOverDue = (data) => {
  return {
    type: Constants.UPDATE_OVERDUE,
    data:data
  }
}

export const MakeCompanyHTML =  ({companyType}) => {
  var returnType = []
  _.forEach(companyType, function(v) {
    returnType.push(type[v])
  })
  return (<div>{returnType.join(', ')}</div>)
}
