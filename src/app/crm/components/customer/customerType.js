const type = require('../../share/type.json')
import React from 'react'

export default ({companyType}) => {
  if(!_.isArray(companyType)) {
    return (<div>{type[companyType]}</div>) 
  }
  var returnType = []
  _.forEach(companyType, function(v) {
    returnType.push(type[v])
  })
  return (<div>{returnType.join(', ')}</div>)
}