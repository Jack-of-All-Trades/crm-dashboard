import React from 'react'
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import { connect } from 'react-redux';
import MenuItem from 'material-ui/MenuItem';
import {Map, List} from 'immutable';
import SortableList from './_customerContact'
import SuperSelectField from 'material-ui-superselectfield'

import AdminUserSelectBar from '../common/adminUserSelectBar';
const moment = require('moment'),
      _ = require('lodash'),
      type = require('../../share/type.json'),
      year = require('../../share/financialYear.json'),
      operation =require("../../share/operation.json"),
      lead = require('../../share/lead.json')

export default ({customer=Map(), logonUser, allUsers, handleValueChange, refList, contactFieldChangeHandle, saveCustomerHandler, customerContacts}) => {
  const Year = ()=> {
    if(refList.has('year')) {
      return refList.get('year').map(function(v) {
        return (
          <MenuItem value={v.id} key={v.id} primaryText={v.value} />
        )
      })
    }
    return null
  }

  const Operation = () => {
    if(refList.has('operation')) {
      return refList.get('operation').map(function(v) {
        return (
          <MenuItem value={v.id}  key={v.id} primaryText={v.value} />
        )
      })
    }
    return null
  }

  var chosenType = []
  var defaultType = customer.has('companyType')?customer.get('companyType'):[]
  if(customer.has('companyType')) {
    customer.get('companyType').map((v)=> {
      chosenType.push({value: v.id, label: v.value})
    })
  }

  const Type = () => {
    if(refList.has('companyType')) {
      return refList.get('companyType').map(function(op) {
        var elem = <span value={op.id} key={op.id} label={op.value}>{op.value}</span>
        return elem
      })
    }
    return null

  }

  var chosenLead = []
  if(customer.has('leads')) {
    customer.get('leads').map((v)=> {
      chosenLead.push({value: v.id, label: v.value})
    })
  }
  const Lead = () => {
    if(refList.has('lead')) {
      return refList.get('lead').map(function(v) {
        return (
          <span value={v.id} key={v.id} label={v.value}>{v.value}</span>
        )
      })
    }
    return false
  }

  if(!customer.get('name')) {
    customer.set('name','')
  }

  if(!customer.has('operationID')) {
    customer = customer.set('operationID', 1)
  }

  if(!customer.has('ownerID')) {
    customer = customer.set('ownerID', logonUser.get('id'))
    customer = customer.set('owner', logonUser.toObject())
  }

  if(!customerContacts) {
    customerContacts = List()
  }
  return (
    <div className="container-fluid">
      <form>
        <div className="row">
          <div className="col-sm-6 form-group">
            <TextField floatingLabelText="Company Name" type="text" onChange={(e,v)=> {
              handleValueChange('name',v)
            }} value={customer.get('name')} defaultValue={customer.get('name')} />
          </div>
          <div className="col-sm-6">
            <SuperSelectField
              name='companyType'
              floatingLabel='Type'
              hintText='Type'
              multiple
              checkPosition='left'
              value={chosenType}
              onChange={(selected,name)=> {
                var chosen = []
                chosenType = []
                _.forEach(selected, function(v) {
                  refList.get('companyType').map((r)=> {
                    if(r.id == v.value)
                      chosen.push(r)
                  })
                  chosenType.push(v)
                })
                handleValueChange('companyType',chosen)
              }}
              style={{marginTop:"35px"}}>
              {Type()}
            </SuperSelectField>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6 form-group">
            <SelectField floatingLabelText="Operation" fullWidth={true} onChange={(e,i,v)=> {
              handleValueChange('operation',_.find(customer.get('operation'), (c)=> {
                return c.id == v
              }))
              handleValueChange('operationID',v)
            }} value={customer.get('operationID')}>
              {Operation()}
            </SelectField>
          </div>
          <div className="col-sm-6">
            <SelectField floatingLabelText="Financial Year" fullWidth={true} onChange={(e,i,v)=> {
              handleValueChange('financialYearID',v)
            }} value={customer.get('financialYearID')}>
              {Year()}
            </SelectField>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6 form-group">
            <SuperSelectField
              name='companyLeadSource'
              floatingLabel='Lead Source'
              hintText='Lead Source'
              multiple
              checkPosition='left'
              value={chosenLead}
              onChange={(selected,name)=> {
                var chosen = []
                chosenLead = []
                _.forEach(selected, function(v) {
                  refList.get('lead').map((r)=> {
                    if(r.id == v.value)
                      chosen.push(r)
                  })
                  chosenLead.push(v)
                })
                handleValueChange('leads',chosen)
              }}
              style={{marginTop:"35px"}}>
              {Lead()}
            </SuperSelectField>
          </div>
          <div className="col-sm-6">
              <AdminUserSelectBar allUsers={allUsers} logonUser={logonUser} placeholder="Lead Owner" allowRoles={["admin","manager"]} onChangeHandler={(value) => {
                var owner = _.find(allUsers, function(u) {
                  return u.id === value
                })
                handleValueChange('owner',owner)
                handleValueChange('ownerID',value)
              }} selectedValue={customer.get('ownerID')} showAll={false} />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12 form-group">
            <SortableList contacts={customerContacts} refList={refList} contactFieldChangeHandle={contactFieldChangeHandle} />
          </div>
        </div>
      </form>
    </div>
  )
}
