import * as ReportContants from './constants'
import {Map, List} from 'immutable'
var moment = require('moment')

const initialState = Map({
  data: [],
  totalDataLoaded: 0,
  stats: [],
  conversion: [],
  companyStats: {
    alpha: [],
    type: [],
    lead: []
  }
})

export default (state=initialState, action) => {  
  let reducer = {}
  reducer[ReportContants.LOADING_GRID] = ({searchObj})=> {    
    return state.set('isLoadingData', true).set('gridSearch',searchObj)
  }
  reducer[ReportContants.LOADED_GRID] = ({data, totalCount})=> {
    return state.set('data',List(data)).set('totalCount', totalCount)
  }
  reducer[ReportContants.LOADING_STATS] = () => {
    return state.set('stats',[])    
  }

  reducer[ReportContants.LOADED_STATS] = ({data}) => {    
    return state.set('stats',data)    
  }

  reducer[ReportContants.LOADING_COMPANYSTATS] = () => {
    return state.set('companyStats',{
      alpha: [],
      type: [],
      lead: []
    })
  }

  reducer[ReportContants.LOADED_COMPANYSTATS] = ({data}) => {
    console.log(data)
    return state.set('companyStats',data)
  }

  reducer[ReportContants.LOADING_CONVERSION] = () => {    
    return state.set('conversion',[])    
  }
  reducer[ReportContants.LOADED_CONVERSION] = ({data}) => {    
    return state.set('conversion',data)    
  }
  reducer['default'] = ()=> {
    return state
  }
  return (reducer[action.type] || reducer['default'])(action)
}