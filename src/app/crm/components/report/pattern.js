// @flow
import React from 'react'
import ReactDOM from 'react-dom'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { Link } from 'react-router-dom';

import ContentAdd from 'material-ui/svg-icons/content/add';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import IconButton from 'material-ui/IconButton';
import {TextField} from 'material-ui'
import * as Scroll from 'react-scroll';

import * as ReportAction from './actions'

import * as gridStyle from '../dashboard/grid.less'

import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

import AdminUserSelectBar from '../common/adminUserSelectBar'

const chance = require('../../share/chance.json'),
			_ = require('lodash'),
      numeral = require('numeral'),
      moment = require('moment')

function priceFormatter(cell, row) {
  return (
    <span>{numeral(cell).format('$ 0,0.00')}</span>
  );
}

function weekPeriodFormatter(cell, row) {
  return (
    <span>{moment().subtract(cell,'w').startOf('W').format("DD-MMM-YY")} - {moment().subtract(cell,'w').endOf('W').format("DD-MMM-YY")}</span>
  );
}

function chancePriceFormatter(cell, row) {
  var returnValue = 0
  switch(cell) {
    case 25: returnValue += (row.latestOpportunity.packagePrice * 0.25)
      break;
    case 26: returnValue += (row.latestOpportunity.packagePrice * 0.5)
      break;
    case 27: returnValue += (row.latestOpportunity.packagePrice * 0.75)
      break;
    case 28: returnValue += (row.latestOpportunity.packagePrice * 1)
      break;
    default: returnValue += (row.latestOpportunity.packagePrice * 0)
      break;
  }
  return (
    <span>{numeral(returnValue).format('$ 0,0.00')}</span>
  );
}

const chanceFormatter = (cell,row, refList)=>{
  var displayText = "25%"
  if(refList.has('chance')) {
    refList.get('chance').map(function(v) {
      if(v.id == cell) {
        displayText = v.value
      }
    })
  }
  return (
    <span>{displayText}</span>
  )
}

function dateFormatter(cell, row) {
  return (
    <span>{moment(cell).format('DD-MMM-YY')}</span>
  );
}


function mapStateToProps(state) {
  return {
    allUsers: state.user.get("allUsers"),
    conversion: state.crm_report.get('conversion'),
    type: state.crm_report.get('type'),
    referenceConfig: state.crm_dashboard.get('referenceConfig'),    
    logonUser: state.Auth.get('userProfile'),
    companyStats: state.crm_report.get('companyStats')
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reportAction: bindActionCreators(ReportAction, dispatch),
    dispatch: dispatch
  }
}

@withRouter
@connect(mapStateToProps, mapDispatchToProps)
export default class PatternReport extends React.Component {
  constructor(props) {
    super(props)
    this.dispatch = props.dispatch
    this.refreshConversion = this.refreshConversion.bind(this)
    this.changeReportUser = this.changeReportUser.bind(this)
    this.refreshCompanyStats = this.refreshCompanyStats.bind(this)
    this.state = {
      reportSearchGrid: {
        userID: 0
      }
    }
  }

  changeReportUser(userID) {
    var oldSearchGridState = _.extend({},this.state.reportSearchGrid, {userID: userID})
		var newState = _.extend({}, this.state, {reportSearchGrid:oldSearchGridState})
    this.setState(newState, ()=> {
      this.refreshConversion()
      this.refreshCompanyStats()    
    })
  }

  componentDidMount() {
    this.refreshConversion()
    this.refreshCompanyStats()
  }

  refreshCompanyStats() {
    var searchGrid = this.state.reportSearchGrid
    let {reportAction} = this.props
    reportAction.loadCompanyStats(searchGrid)
  }


  refreshConversion() {
    var searchGrid = this.state.reportSearchGrid
    let {reportAction} = this.props
    reportAction.loadConversion(searchGrid,13)
  }

  reverseData(data) {
    var returnData = []
    var reverseData = []
    var row = {"name":"Conversion","totalSales":"Total Sales",'totalPkg':"Total Package",'conversation':"Total Conversation",'aveSale':"Avg sale per package",'aveSalesPerConversation':"Sales to Conversations",'aveConversationPerPkg':"Conversations needed per package"}
    _.forEach(data, function(_data) {
      returnData[_data.period] = {
        name: moment().subtract(_data.period,'months').format('MMM-YY'),
        totalSales: _data.totalSales,
        totalPkg: _data.totalPkg,
        conversation: _data.totalConversation,
        aveSale: (_data.totalPkg==0)?0:(_data.totalSales/_data.totalPkg),
        aveSalesPerConversation: (_data.totalConversation==0)?0:_data.totalSales/_data.totalConversation,
        aveConversationPerPkg: (_data.totalPkg==0)?0:(_data.totalConversation/_data.totalPkg)
      }
    })

    _.forEach(row, function(v,k) {
      var _rowData = {}
      _rowData['name'] = v
      _.forEach(returnData, function(_data, key) {
        if(k=="totalSales" || k=="aveSale" || k=="aveSalesPerConversation") {
          _rowData[moment().subtract(key, 'months').format('YYYYMMDD')] = numeral(_data[k]).format('$ 0,0.00')
        } else {
          _rowData[moment().subtract(key, 'months').format('YYYYMMDD')] = _data[k]
        }
      })
      reverseData.push(_rowData)
    })
    
    return reverseData
  }

  render() {
    let {conversion, logonUser, allUsers, companyStats} = this.props    
    var conversionData = this.reverseData(conversion)
    const columns = [];
    var settingUpCol = conversionData.shift();
    columns.push({
      dataField: 'name',
      text: "Conversion"
    })
    delete settingUpCol['name']
    var keys = Object.keys( settingUpCol );
    keys.reverse()
    _.forEach(keys, function(key) {
      columns.push({
        dataField: key,
        text: settingUpCol[key]
      })
    })
    const statsCol = [
      {
        dataField: 'period',
        text: 'Week',
        formatter: weekPeriodFormatter
      },
      {
        dataField: 'conversation.totalCount',
        text: 'Conversation'
      }, 
      {
        dataField: 'conversation.average',
        text: 'Avg Conversations per day'        
      }, 
      {
        dataField: 'pitchCount.new',
        text: 'New'
      }, 
      {
        dataField: 'pitchCount.prospecting',
        text: 'Prospecting'
      }, 
      {
        dataField: 'pitchCount.proposed',
        text: 'Proposed'
      }, 
      {
        dataField: 'pitchCount.won',
        text: 'Won'
      }, 
      {
        dataField: 'pitchCount.notKeen',
        text: 'Not Keen'
      }
    ];

    var numArray = '0123456789'.split(''),
        numData = []
    numArray.map(function(v) {
      if(companyStats['alpha'].hasOwnProperty(v.toString())) {
        
        numData.push(companyStats['alpha'][v.toString()])
      } else {
        numData.push({
          name: v,
          new: "(no input from user)",
          prospecting: "(no input from user)",
          proposed: "(no input from user)",
          won: "(no input from user)",
          notKeen: "(no input from user)"
        })
      }
    })

    var numCharCol = [
      {
        text: "#",
        dataField: 'name'
      },
      {
        text: "New",
        dataField: 'new'
      },
      {
        text: "Prospecting",
        dataField: 'prospecting'
      },
      {
        text: "Proposed",
        dataField: 'proposed'
      },
      {
        text: "Won",
        dataField: 'won'
      },
      {
        text: "Not Keen",
        dataField: 'notKeen'
      }
    ]

    var charArray = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
        charData = []
    charArray.map(function(v) {
      if(companyStats['alpha'].hasOwnProperty(v)) {
        charData.push(companyStats['alpha'][v])
      } else {
        charData.push({
          name: v,
          new: "(no input from user)",
          prospecting: "(no input from user)",
          proposed: "(no input from user)",
          won: "(no input from user)",
          notKeen: "(no input from user)"
        })
      }
    })

    var alphaCharCol = [
      {
        text: "Alphabet",
        dataField: 'name'
      },
      {
        text: "New",
        dataField: 'new'
      },
      {
        text: "Prospecting",
        dataField: 'prospecting'
      },
      {
        text: "Proposed",
        dataField: 'proposed'
      },
      {
        text: "Won",
        dataField: 'won'
      },
      {
        text: "Not Keen",
        dataField: 'notKeen'
      }
    ]

    var allCompanyTypeData = [] 
    this.props.referenceConfig.get('companyType').map(function(v) {
      if(v.value)
      if(companyStats['type'].hasOwnProperty(v.value)) {
        allCompanyTypeData.push(companyStats['type'][v.value])
      } else {
        allCompanyTypeData.push({
          name: v.value,
          new: "(no input from user)",
          prospecting: "(no input from user)",
          proposed: "(no input from user)",
          won: "(no input from user)",
          notKeen: "(no input from user)"
        })
      }
    })
    allCompanyTypeData = _.orderBy(allCompanyTypeData, 'name','asc')


    var allLeadTypeData = [] 
    this.props.referenceConfig.get('lead').map(function(v) {
      if(companyStats['lead'].hasOwnProperty(v.value)) {
        allLeadTypeData.push(companyStats['lead'][v.value])
      } else {
        allLeadTypeData.push({
          name: v.value,
          new: "(no input from user)",
          prospecting: "(no input from user)",
          proposed: "(no input from user)",
          won: "(no input from user)",
          notKeen: "(no input from user)"
        })
      }
    })
    allLeadTypeData = _.orderBy(allLeadTypeData, 'name','asc')
    

    var companyLeadCol = [
      {
        text: "Lead",
        dataField: 'name'
      },
      {
        text: "New",
        dataField: 'new'
      },
      {
        text: "Prospecting",
        dataField: 'prospecting'
      },
      {
        text: "Proposed",
        dataField: 'proposed'
      },
      {
        text: "Won",
        dataField: 'won'
      },
      {
        text: "Not Keen",
        dataField: 'notKeen'
      }
    ]
    var companyTypeCol = [
      {
        text: "Type",
        dataField: 'name'
      },
      {
        text: "New",
        dataField: 'new'
      },
      {
        text: "Prospecting",
        dataField: 'prospecting'
      },
      {
        text: "Proposed",
        dataField: 'proposed'
      },
      {
        text: "Won",
        dataField: 'won'
      },
      {
        text: "Not Keen",
        dataField: 'notKeen'
      }
    ]

    return (
    <div className="col-md-12">
      <article className="article">
        <h2 className="article-title">Report</h2>        
        <RaisedButton containerElement={<Link to="/crm" replace  />} labelPosition="after" icon={<ActionAssignment />} primary={true} label="Dashboard" />{" "}
        <RaisedButton containerElement={<Link to="/crm/report" replace  />} labelPosition="after" icon={<ActionAssignment />} primary={true} label="Report" />{" "}
        <RaisedButton containerElement={<Link to="/crm/report/pattern" replace  />} labelPosition="after" icon={<ActionAssignment />} primary={true} label="Pattern Report" />{" "}
        <AdminUserSelectBar logonUser={logonUser} onChangeHandler={this.changeReportUser} allowRoles={["admin","manager"]} allUsers={allUsers} selectedValue={this.state.reportSearchGrid.userID} />        
        <BootstrapTable data={numData} columns={numCharCol} keyField="name"/>
        <BootstrapTable data={charData} columns={alphaCharCol} keyField="name"/>
        <BootstrapTable data={allLeadTypeData} columns={companyLeadCol} keyField="name"/>
        <BootstrapTable data={allCompanyTypeData} columns={companyTypeCol} keyField="name"/>
        <BootstrapTable data={conversionData} columns={columns} keyField="name"/>
      </article>
    </div>
    )
  }
}
