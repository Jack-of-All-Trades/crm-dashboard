import * as ReportContants from './constants'
import axios from '../../services/request'
import {Map, List} from 'immutable'
import parallelLimit from 'async/parallelLimit';

const moment = require('moment')


const querystring = require('querystring')

export function loadList(gridSearch) {
  return (dispatch) => {
    dispatch({
      type: ReportContants.LOADING_GRID      
    });
    axios.get('/crm/company', {params: gridSearch})
      .then((data)=>{
        dispatch({
          type: ReportContants.LOADED_GRID,
          data: data.data,
          totalCount: Number(data.headers['x-total-length'])
        })
      })
  }
}

function calLatestPitch(data) {
  var companyList = Map()
  var returnValue = {
    new: 0,
    prospecting:0,
    proposed: 0,
    won: 0,
    notKeen: 0
  }
  for(var company in data) {
    if(!companyList.has("c"+company.companyID)) {
      companyList = companyList.set("c"+company.companyID, company)
    }
    var checkCompany = companyList.get("c"+company.companyID)
    if(moment(checkCompany.addedDate).isBefore(company.addedDate)) {
      companyList = companyList.set("c"+company.companyID, company)
    }
  }
  if(companyList.count()>0) {
    companyList.map(function(v) {
      switch(v.pitchStatusID) {                
        case 16: returnValue.prospecting += 1
          break;
        case 17: returnValue.proposed += 1
          break;
        case 18: returnValue.won += 1
          break;
        case 19: returnValue.notKeen += 1
          break;
        default: returnValue.new += 1
          break;
      }
    })
  }
  return returnValue
}

export function loadCompanyStats(gridSearch, period) {
  return (dispatch) => {
    dispatch({
      type: ReportContants.LOADING_COMPANYSTATS      
    });
    
    var weekly = {}
    weekly['alpha'] = function(cb) {
      axios.get('/crm/company', {params: {statsPitchStatus:1, userID:gridSearch.userID, statsCompanyFirstChar: 1, limit: -1}})
        .then((data)=>{
          var returnValue = {}
          var returnStatus = {
            new: 0,
            prospecting:0,
            proposed: 0,
            won: 0,
            notKeen: 0
          }
          if(data.data && data.data.length>0) {
            for(var o=0;data.data.length>o;o++) {
              var currentV = data.data[o]
              var key = _.toUpper(currentV.companyAlpha)
              if(!returnValue.hasOwnProperty(key)) {
                returnValue[key] = {
                  new: 0,
                  prospecting:0,
                  proposed: 0,
                  won: 0,
                  notKeen: 0,
                  name: key
                }
              }
              switch(currentV.pitchStatusID) {                
                case 16: returnValue[key].prospecting = currentV.statusCount
                  break;
                case 17: returnValue[key].proposed = currentV.statusCount
                  break;
                case 18: returnValue[key].won = currentV.statusCount
                  break;
                case 19: returnValue[key].notKeen = currentV.statusCount
                  break;
                default: returnValue[key].new = currentV.statusCount
                  break;
              }
            }
          }
          cb(null,returnValue)          
        })
    }
    weekly['lead'] = function(cb) {
      axios.get('/crm/company', {params: {statsPitchStatus:1, userID:gridSearch.userID, statsLeadType: 1, limit: -1}})
        .then((data)=>{
          var returnValue = {}
          var returnStatus = {
            new: 0,
            prospecting:0,
            proposed: 0,
            won: 0,
            notKeen: 0
          }
          if(data.data && data.data.length>0) {
            for(var o=0;data.data.length>o;o++) {
              var currentV = data.data[o]
              if(!returnValue.hasOwnProperty(currentV.companyLeadValue)) {
                returnValue[currentV.companyLeadValue] = {
                  new: 0,
                  prospecting:0,
                  proposed: 0,
                  won: 0,
                  notKeen: 0,
                  name: currentV.companyLeadValue
                }
              }
              switch(currentV.pitchStatusID) {                
                case 16: returnValue[currentV.companyLeadValue].prospecting = currentV.statusCount
                  break;
                case 17: returnValue[currentV.companyLeadValue].proposed = currentV.statusCount
                  break;
                case 18: returnValue[currentV.companyLeadValue].won = currentV.statusCount
                  break;
                case 19: returnValue[currentV.companyLeadValue].notKeen = currentV.statusCount
                  break;
                default: returnValue[currentV.companyLeadValue].new = currentV.statusCount
                  break;
              }
            }
          }
          cb(null,returnValue)          
        })
    }

    weekly['type'] = function(cb) {
      axios.get('/crm/company', {params: {statsPitchStatus:1, userID:gridSearch.userID, statsCompanyType: 1, limit: -1}})
        .then((data)=>{
          var returnValue = {}
          var returnStatus = {
            new: 0,
            prospecting:0,
            proposed: 0,
            won: 0,
            notKeen: 0
          }
          if(data.data && data.data.length>0) {
            for(var o=0;data.data.length>o;o++) {
              var currentV = data.data[o]
              if(!returnValue.hasOwnProperty(currentV.companyTypeValue)) {
                returnValue[currentV.companyTypeValue] = {
                  new: 0,
                  prospecting:0,
                  proposed: 0,
                  won: 0,
                  notKeen: 0,
                  name: currentV.companyTypeValue
                }
              }
              switch(currentV.pitchStatusID) {                
                case 16: returnValue[currentV.companyTypeValue].prospecting = currentV.statusCount
                  break;
                case 17: returnValue[currentV.companyTypeValue].proposed = currentV.statusCount
                  break;
                case 18: returnValue[currentV.companyTypeValue].won = currentV.statusCount
                  break;
                case 19: returnValue[currentV.companyTypeValue].notKeen = currentV.statusCount
                  break;
                default: returnValue[currentV.companyTypeValue].new = currentV.statusCount
                  break;
              }
            }
          }
          cb(null,returnValue)          
        })
    }
    parallelLimit(weekly, 2, function(err,d) {
      console.log(d)
      dispatch({
        type: ReportContants.LOADED_COMPANYSTATS,
        data: d
      })
    })
  }
}

export function loadConversion(gridSearch, period) {
  return (dispatch) => {
    dispatch({
      type: ReportContants.LOADING_CONVERSION      
    });

    var weekly = []    
    for(var i=0; i<period; i++) {
      (function (ii) {
        weekly.push(function(cb) {
          axios.get('/crm/conversation/Company', {
              params: {
                count:1,
                userID: gridSearch.userID, 
                endDate: moment().subtract(ii, "months").endOf("month").toDate(), 
                startDate: moment().subtract(ii, "months").startOf('month').toDate()
              }
            })
            .then((data)=>{              
              var average = 0;
              if(data.data>0)
                average = data.data/5
              cb(null, {
                totalConversation: data.data,                
                period: ii
              })
            })
        })
        weekly.push(function(cb) {
          axios.get('/crm/opportunity', {
              params: {
                userID: gridSearch.userID, 
                endCampaignStartDate: moment().subtract(ii, "months").endOf("month").toDate(), 
                startCampaignStartDate: moment().subtract(ii, "months").startOf('month').toDate(),
                chanceID: 28
              }
            })
            .then((data)=>{              
              cb(null, {
                totalPkg: data.data[0].totalPkg,
                totalSales: data.data[0].totalAmt == null?0:data.data[0].totalAmt,
                period: ii
              })        
            })
        })
      })(i)
    }
    parallelLimit(weekly, 2, function(err,d) {
      var returnData = []
      _.forEach(d, function(v) {
        var foundData = _.findIndex(returnData, function(c) {
          return c.period == v.period
        })        
        if(foundData>=0) {
          returnData[foundData] = _.extend({}, returnData[foundData], v)          
        } else {
          returnData.push(v)
        }
      })
      dispatch({
        type: ReportContants.LOADED_CONVERSION,
        data: returnData
      });
    })
  }
}

export function loadStats(gridSearch, period) {
  return (dispatch) => {
    dispatch({
      type: ReportContants.LOADING_STATS      
    });

    var weekly = []    
    for(var i=0; i<period; i++) {
      (function (ii) {
        weekly.push(function(cb) {
          axios.get('/crm/conversation/Company', {
              params: {
                count:1,
                userID: gridSearch.userID, 
                endDate: moment().subtract(ii, "w").endOf("week").toDate(), 
                startDate: moment().subtract(ii, "w").startOf('week').toDate()
              }
            })
            .then((data)=>{              
              var average = 0;
              if(data.data>0)
                average = data.data/5
              cb(null, {
                conversation: {
                  totalCount: data.data,
                  average: average
                },                
                period: ii
              })
            })
        })
        weekly.push(function(cb) {
          axios.get('/crm/companyPitches', {
              params: {
                ownerID: gridSearch.userID, 
                endDate: moment().subtract(ii, "W").endOf("week").toDate(), 
                startDate: moment().subtract(ii, "W").startOf('week').toDate()
              }
            })
            .then((data)=>{              
              var companyList = Map()
              var returnValue = {
                new: 0,
                prospecting:0,
                proposed: 0,
                won: 0,
                notKeen: 0
              }
              for(var company in data.data) {
                if(!companyList.has("c"+company.companyID)) {
                  companyList = companyList.set("c"+company.companyID, company)
                }
                var checkCompany = companyList.get("c"+company.companyID)
                if(moment(checkCompany.addedDate).isBefore(company.addedDate)) {
                  companyList = companyList.set("c"+company.companyID, company)
                }
              }
              if(companyList.count()>0) {
                companyList.map(function(v) {
                  switch(v.pitchStatusID) {                
                    case 16: returnValue.prospecting += 1
                      break;
                    case 17: returnValue.proposed += 1
                      break;
                    case 18: returnValue.won += 1
                      break;
                    case 19: returnValue.notKeen += 1
                      break;
                    default: returnValue.new += 1
                      break;
                  }
                })
              }
              cb(null, {
                pitchCount: returnValue,
                period: ii
              })          
            })
        })
      })(i)
    }
    parallelLimit(weekly, 2, function(err,d) {
      var returnData = []
      _.forEach(d, function(v) {
        var foundData = _.findIndex(returnData, function(c) {
          return c.period == v.period
        })        
        if(foundData>=0) {
          returnData[foundData] = _.extend({}, returnData[foundData], v)          
        } else {
          returnData.push(v)
        }
      })
      dispatch({
        type: ReportContants.LOADED_STATS,
        data: returnData
      });
    })
  }
}