// @flow
import React from 'react'
import ReactDOM from 'react-dom'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { Link } from 'react-router-dom';

import ContentAdd from 'material-ui/svg-icons/content/add';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import IconButton from 'material-ui/IconButton';
import {TextField} from 'material-ui'
import * as Scroll from 'react-scroll';

import * as ReportAction from './actions'

import * as gridStyle from '../dashboard/grid.less'

import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

import AdminUserSelectBar from '../common/adminUserSelectBar'

const chance = require('../../share/chance.json'),
			_ = require('lodash'),
      numeral = require('numeral'),
      moment = require('moment')

function priceFormatter(cell, row) {
  return (
    <span>{numeral(cell).format('$ 0,0.00')}</span>
  );
}

function weekPeriodFormatter(cell, row) {
  return (
    <span>{moment().subtract(cell,'w').startOf('W').format("DD-MMM-YY")} - {moment().subtract(cell,'w').endOf('W').format("DD-MMM-YY")}</span>
  );
}

function chancePriceFormatter(cell, row) {
  var returnValue = 0
  switch(cell) {
    case 25: returnValue += (row.latestOpportunity.packagePrice * 0.25)
      break;
    case 26: returnValue += (row.latestOpportunity.packagePrice * 0.5)
      break;
    case 27: returnValue += (row.latestOpportunity.packagePrice * 0.75)
      break;
    case 28: returnValue += (row.latestOpportunity.packagePrice * 1)
      break;
    default: returnValue += (row.latestOpportunity.packagePrice * 0)
      break;
  }
  return (
    <span>{numeral(returnValue).format('$ 0,0.00')}</span>
  );
}

const chanceFormatter = (cell,row, refList)=>{
  var displayText = "25%"
  if(refList.has('chance')) {
    refList.get('chance').map(function(v) {
      if(v.id == cell) {
        displayText = v.value
      }
    })
  }
  return (
    <span>{displayText}</span>
  )
}

function dateFormatter(cell, row) {
  if(!cell) {
    return <span> - </span>
  }
  return (
    <span>{moment(cell).format('DD-MMM-YY')}</span>
  );
}


function mapStateToProps(state) {
  return {
    allUsers: state.user.get("allUsers"),
    data: state.crm_report.get('data'),
    stats: state.crm_report.get('stats'),
    referenceConfig: state.crm_dashboard.get('referenceConfig'),
    totalCount: state.crm_report.get('totalDataLoaded'),
    logonUser: state.Auth.get('userProfile')
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reportAction: bindActionCreators(ReportAction, dispatch),
    dispatch: dispatch
  }
}

@withRouter
@connect(mapStateToProps, mapDispatchToProps)
export default class Report extends React.Component {
  constructor(props) {
    super(props)
    this.dispatch = props.dispatch
    this.refreshGrid = this.refreshGrid.bind(this)
    this.changeReportUser = this.changeReportUser.bind(this)

    this.state = {
      reportSearchGrid: {
        userID: 0,
        search: "",
        chanceIDs: "25,26,27",
        includeNoOpporunity: 0,
        sort:"name ASC", 
        page:1, 
        limit:30
      }
    }
  }

  changeReportUser(userID) {
    var oldSearchGridState = _.extend({},this.state.reportSearchGrid, {userID: userID})
		var newState = _.extend({}, this.state, {reportSearchGrid:oldSearchGridState})
    this.setState(newState, ()=> {
      this.refreshGrid()
      this.refreshStats()
    })
  }

  componentDidMount() {
    this.refreshGrid()
    this.refreshStats()
  }

  refreshStats() {
    var searchGrid = this.state.reportSearchGrid
    let {reportAction} = this.props
    reportAction.loadStats(searchGrid, 12)
  }


  refreshGrid() {
    var searchGrid = this.state.reportSearchGrid
    let {reportAction} = this.props
    reportAction.loadList(searchGrid)
  }

  render() {
    let {data, logonUser, allUsers, stats} = this.props

    const columns = [
      {
        dataField: 'name',
        text: 'Name'
      },
      {
        dataField: 'latestOpportunity.packagePrice',
        text: 'Package',
        formatter: priceFormatter
      }, 
      {
        dataField: 'latestOpportunity.chanceID',
        text: 'Chance',
        formatter: (col,row)=> chanceFormatter(col,row,this.props.referenceConfig)
      },
      {
        dataField: 'latestOpportunity.chanceID',
        text: 'Package:Chance',
        formatter: chancePriceFormatter
      }, 
      {
        dataField: 'latestConversation.addedDate',
        text: 'Last Conversation',
        formatter: dateFormatter
      }, 
      {
        dataField: 'followUpDate',
        text: 'FUP Date',
        formatter: dateFormatter
      }
    ];

    const statsCol = [
      {
        dataField: 'period',
        text: 'Week',
        formatter: weekPeriodFormatter
      },
      {
        dataField: 'conversation.totalCount',
        text: 'Conversation'
      }, 
      {
        dataField: 'conversation.average',
        text: 'Avg Conversations per day'        
      }, 
      {
        dataField: 'pitchCount.new',
        text: 'New'
      }, 
      {
        dataField: 'pitchCount.prospecting',
        text: 'Prospecting'
      }, 
      {
        dataField: 'pitchCount.proposed',
        text: 'Proposed'
      }, 
      {
        dataField: 'pitchCount.won',
        text: 'Won'
      }, 
      {
        dataField: 'pitchCount.notKeen',
        text: 'Not Keen'
      }
    ];

    return (
    <div className="col-md-12">
      <article className="article">
        <h2 className="article-title">Report</h2>
        <RaisedButton containerElement={<Link to="/crm" replace  />} labelPosition="after" icon={<ActionAssignment />} primary={true} label="Dashboard" />{" "}
        <RaisedButton containerElement={<Link to="/crm/report" replace  />} labelPosition="after" icon={<ActionAssignment />} primary={true} label="Report" />{" "}
        <RaisedButton containerElement={<Link to="/crm/report/pattern" replace  />} labelPosition="after" icon={<ActionAssignment />} primary={true} label="Pattern Report" />{" "}
        <AdminUserSelectBar logonUser={logonUser} onChangeHandler={this.changeReportUser} allowRoles={["admin","manager"]} allUsers={allUsers} selectedValue={this.state.reportSearchGrid.userID} />
        <BootstrapTable data={data} columns={columns} keyField="id"/>
        <BootstrapTable data={stats} columns={statsCol} keyField="period"/>
      </article>
    </div>
    )
  }
}
