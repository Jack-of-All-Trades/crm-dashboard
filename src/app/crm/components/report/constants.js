export const LOADING_GRID = "module_cms_report_grid_loading"
export const LOADED_GRID = "module_cms_report_grid_loaded"
export const LOADING_STATS = "module_cms_report_loading_stats"
export const LOADED_STATS = "module_cms_report_loaded_stats"

export const LOADING_CONVERSION = "module_cms_report_loading_conversion"
export const LOADED_CONVERSION = "module_cms_report_loaded_conversion"
export const LOADING_COMPANYSTATS = "module_cms_report_loading_companyStats"
export const LOADED_COMPANYSTATS = "module_cms_report_loaded_companyStats"