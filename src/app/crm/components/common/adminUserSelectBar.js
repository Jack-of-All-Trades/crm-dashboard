import React from 'react'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const _ = require('lodash')

const OwnerMenuItems = (list, showAll)=> {
  var newList = _.clone(list)
  if(showAll) {
    newList.unshift({
      id:0,
      profile: {
        displayName: "All User"
      }
    })
  }
  
  return newList.map(function(k) {
    return (
      <MenuItem value={k.id} key={k.id} primaryText={k.profile.displayName}/>
    )
  })
}

const AdminUserSelectBar = ({logonUser, allowRoles, onChangeHandler, allUsers, selectedValue, placeholder="Report for User", showAll=true}) => {
  var roles = logonUser.get('roles')
  var isAdmin = false
  _.forEach(roles, function(role) {
    if(role.moduleName == "crm") {
      var found = _.find(allowRoles, function(a) {
        return role.name == a
      })
      if(found) {
        isAdmin = true
      }
    }
  })
  if(!isAdmin)
    return null
  else {
    var _onChangeHandler = (e,i,v) => {
      onChangeHandler(v)
    }
    return (
      <div className="row">
        <div className="col-12">
          <SelectField onChange={_onChangeHandler} value={selectedValue}
            fullWidth={true}
            floatingLabelText={placeholder}
            >
              {OwnerMenuItems(allUsers,showAll)}
            </SelectField>
        </div>
      </div>)
  }
}



export default AdminUserSelectBar