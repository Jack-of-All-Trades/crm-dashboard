import React from 'react'

class Sidebar extends React.Component {

  constructor(props) {
  }

  componentDidMount() {    
  }

  render() {
    
  }
}

const mapStateToProps = (state,ownProps) => ({
  modulesLoaded:state.Modules.loadedModule || false,
  modules: ownProps.modules
});

var SideNav = connect(mapStateToProps)(Sidebar);

export default SideNav