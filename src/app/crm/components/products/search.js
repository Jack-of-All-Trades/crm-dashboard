import React from 'react'
import {Card, CardActions, CardText, CardTitle} from 'material-ui/Card'
import TextField from 'material-ui/TextField';
import classNames from 'classnames'
import { Link } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import Style from './search.scss';


export default function Search({search, cancelFunction}) {
	return (		
		<Card className={classNames(Style.topSection, "container")}>
			<CardTitle title="Product Search"/>
			<CardText className="container">
				<form className="row">
					<div className="form-group col-md-4">
						<TextField id="productName"
							floatingLabelText="By Product Name"
							fullWidth={true} />
				  </div>
				  <div className="form-group col-md-4">
				    <TextField id="productBrand"
							floatingLabelText="By Brands"
							fullWidth={true} />
				  </div>
				</form>
			</CardText>
			<CardActions>
				<RaisedButton 
					className={Style.resetBackBtn}
					containerElement={<Link to={{
							pathname: '/products',
							search: search
						}}/>}
					primary={true} 
					label="Search" />
	      <RaisedButton 
	      	href="#" 
	      	onClick={cancelFunction}
	      	label="Clear" />
			</CardActions>
		</Card>
	)
}

