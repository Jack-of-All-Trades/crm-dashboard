import React from 'react'
import Search from './products/search'

export default ()=>{
	return (
		<section>
			<Search />
		</section>
	)
}