import React from 'react'
import { Link } from 'react-router-dom';
const _ = require('lodash'),
      moment = require('moment')


const LatestConversation = ({communication, companyID}) => {
  if(_.isEmpty(communication)) {
    return <Link to={`/crm/communication/${companyID}`} >Add/View</Link> 
  }
  return (
    <Link to={`/crm/communication/${companyID}`}>{communication.value}</Link>
  )
}

export default LatestConversation