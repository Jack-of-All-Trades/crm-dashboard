import React from 'react'
import {Map} from 'immutable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as UserActions  from '../../../base/components/User/actions'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const _ = require('lodash')

function mapStateToProps(state) {
  return {
    auth: state.Auth.get('userProfile')
  };
}

@connect(mapStateToProps)
export default class DisplayOwner extends React.Component {
  constructor(props) {
    super(props)
    this.userActions = bindActionCreators(UserActions, props.dispatch)
    var roles = this.props.auth.get('roles')
    this.isAdmin = false
    this.company = props.company
    this.onChangeHandler = this.onChangeHandler.bind(this)
    var self = this
    _.forEach(roles, function(role) {
      if(role.moduleName == "crm" && role.name == "admin")
        self.isAdmin = true
      if(role.moduleName == "crm" && role.name == "manager")
        self.isAdmin = true
    })
    this.OwnerMenuItems = (list)=> {
      return list.map(function(k) {
        return (
          <MenuItem value={k.id} key={k.id} primaryText={k.profile.displayName} style={{"font-size":"12px", "color":"#0a6ebd"}}/>
        )
      })
    }
    this.state = {
      updatingOwner: false,
    }
  }

  onChangeHandler(e,i,v) {
		e.preventDefault()
    var self = this
    if(this.company.ownerID != v) {
      this.setState((prevState)=> {
        self.company.ownerID = v
        var saveCompany = _.clone(self.company)
        delete saveCompany['owner']
        self.props.updateCompany(saveCompany, saveCompany.id, function() {
          self.setState((prevState)=> {
            return _.extend({}, prevState, {updatingOwner: false})
          })
        })
        return _.extend({}, prevState, {updatingOwner: true})
      })
    } 
	}

  render() {
    var displayValue = "User not found"
    if(this.props.value == this.props.auth.get('id'))
      displayValue = "You"
    else {
      for(var user of this.props.allUsers) {
        if(user.id == this.props.value) {
          displayValue = user.profile.displayName
        }
      }
    }
    if(this.isAdmin && this.props.company.operationID != 2) {
      if(!this.state.updatingOwner) {
        return (<SelectField onChange={this.onChangeHandler} value={this.props.value}
          style={{"font-size":"12px" }}
          labelStyle={{"color":"#0a6ebd"}}
          >
            {this.OwnerMenuItems(this.props.allUsers)}
          </SelectField>)
      } else {
        return(<React.Fragment>
          {displayValue}
        </React.Fragment>)
      }
      
    } else {
      return (
        <React.Fragment>
          {displayValue}
        </React.Fragment>
      )
    }
  }
}
