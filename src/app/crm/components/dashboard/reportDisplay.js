import React from 'react'
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import DatePicker from 'material-ui/DatePicker';
import NumberFormat from 'react-number-format';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const moment = require('moment')

const OwnerMenuItems = (list)=> {
  var newList = _.clone(list)
  newList.unshift({
    id:0,
    profile: {
      displayName: "All User"
    }
  })
  return newList.map(function(k) {
    // return (
    //   //<MenuItem value={k.id} key={k.id} primaryText={k.profile.displayName}> </MenuItem>
    // )
  })
}


const adminBar = (logonUser, onChangeHandler, allUsers, selectedValue) => {
  var roles = logonUser.get('roles')
  var isAdmin = false
  _.forEach(roles, function(role) {
    if(role.moduleName == "crm" && role.name == "admin")
      isAdmin = true
    if(role.moduleName == "crm" && role.name == "manager")
      isAdmin = true
  })
  if(!isAdmin)
    return null
  else {
    return (
      <div className="row">
        <div className="col-12">
          <SelectField onChange={onChangeHandler} value={selectedValue}
            fullWidth={true}
            floatingLabelText="Report for User"
            >
              {OwnerMenuItems(allUsers)}
            </SelectField>
        </div>
      </div>)
  }
}

const ReportDisplay = ({stats, allUsers, logonUser, loadStats, reportSelectedValue}) => {
  var onChangeHandler = (e,i,v) => {
    loadStats(v)
  }

  var status = {
    new: 0,
    prospecting:0,
    proposed: 0,
    won: 0,
    notKeen: 0
  }
  if(stats.status) {
    status = stats.status
  }
  return (
    <div>
      {adminBar(logonUser, onChangeHandler, allUsers, reportSelectedValue)}
      <div className="row">
        <div className="col-6">
          <div className="row">
            <div className="col-8">
              Follow-ups today
            </div>
            <div className="col-4">
              {stats.today}
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              Follow-ups in the next 7 days
            </div>
            <div className="col-4">
              {stats.sevenDays}
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              New / Yet to contact
            </div>
            <div className="col-4">              
              {status.new}
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              Prospecting
            </div>
            <div className="col-4">
              {status.prospecting}
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              Proposed
            </div>
            <div className="col-4">
              {status.proposed}
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              Not keen, follow up next time
            </div>
            <div className="col-4">
              {status.notKeen}
            </div>
          </div>
        </div>
        <div className="col-6">
          <div className="row">
            <div className="col-8">
              Pipeline
            </div>
            <div className="col-4">
              <NumberFormat value={stats.pipelineWon} displayType={'text'} thousandSeparator={true} prefix={'$'} decimalScale={2} fixedDecimalScale={true}/>              
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              Pipeline based on chance
            </div>
            <div className="col-4">
              <NumberFormat value={stats.pipelineChance} displayType={'text'} thousandSeparator={true} prefix={'$'}  decimalScale={2} fixedDecimalScale={true}/>              
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              This month's actions
            </div>
            <div className="col-4">
              {stats.thisMonth}
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              Last month's actions
            </div>
            <div className="col-4">
              {stats.lastMonth}
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              This month's sales
            </div>
            <div className="col-4">
              <NumberFormat value={stats.thisMonthSales} displayType={'text'} allowEmptyFormatting={true} thousandSeparator={true} prefix={'$'}  decimalScale={2} fixedDecimalScale={true}/>                            
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              Last month's sales
            </div>
            <div className="col-4">
              <NumberFormat value={stats.lastMonthSales} displayType={'text'} allowEmptyFormatting={true} thousandSeparator={true} prefix={'$'}  decimalScale={2} fixedDecimalScale={true}/>                            
            </div>
          </div>
        </div>
      </div>      
    </div>
  )
}

export default ReportDisplay