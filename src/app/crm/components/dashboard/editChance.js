import React from 'react'
import { bindActionCreators } from 'redux';
import * as OpportunityActions from '../opportunity/action'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {Map} from 'immutable';
import { Link } from 'react-router-dom';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

const chance = require('../../share/chance.json'),
			_ = require('lodash'),
			numeral = require('numeral')

const StageMenuItems = ()=> {
  return _.keys(chance).map(function(k) {
    return (
      <MenuItem value={k} key={"chance-"+k} primaryText={chance[k]} />
    )
  })
}

export default class EditChance extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			editing: false,
			value: _.isEmpty(this.props.opportunity)?'25%':this.props.opportunity.chance,
			opportunity: this.props.opportunity,
			remark: "",
			showRemark: false,
			remarkError: null
		}
		this.opportunity = this.props.opportunity
		this.onClickHandler = this.onClickHandler.bind(this)
		this.onChangeHandler = this.onChangeHandler.bind(this)
		this.updateRemark = this.updateRemark.bind(this)
		this.updateOpportunity = this.updateOpportunity.bind(this)
		this.opportunityActions = bindActionCreators(OpportunityActions, props.dispatch)
		this.actions = [
      <FlatButton
        label="Save"
        primary={true}
        onClick={this.updateOpportunity}
      />
		];
	}

	onChangeHandler(e,i,v) {
		var remark = false
		if(v == "won" || v == "lost") {
			remark = true
		}
		this.setState((prevState)=> {
			return _.extend({}, prevState, {editing: false, showRemark:remark})
		})
		if(!remark)
			this.updateOpportunity()
	}

	onClickHandler() {
		this.setState((prevState)=> {
			return _.extend({}, prevState, {editing: true})
		})
	}

	updateOpportunity() {
		var self = this
		this.setState((prevState)=> {
			return _.extend({}, prevState, {remarkError: null})
		})
		var savedData = {
			_id: this.opportunity._id,
			chance: this.state.value
		}
		if(this.state.showRemark) {
			if(_.isEmpty(this.state.remark)) {
				return this.setState((prevState)=> {
					return _.extend({}, prevState, {remarkError: "Remark cannot be empty"})
				})
			}
			savedData['remark'] = this.state.remark
		}

    this.opportunityActions.save(Map(savedData), function(data) {
      self.setState((prevState)=> {
				return _.extend({}, prevState, {value: value})
			})
    })
    this.setState((prevState)=> {
			return _.extend({}, prevState, {value: value, showRemark: false})
		})
	}

	updateRemark(e,v) {
		this.setState((prevState)=> {
			return _.extend({}, prevState, {remark: v})
		})
	}

	render() {
		var {value, opportunity, companyID, refList} = this.props

		if(_.isEmpty(opportunity)) {
			return <Link to={`/crm/opportunity/${companyID}`} >New Opportunity</Link>
		} else {
			var displayChance = ""
			if(refList.has('chance')) {
				refList.get('chance').map((v) => {
					if(v.id == opportunity.chanceID) {
						displayChance = v.value
					}
				})
			}
			return (
				<div>
					<Dialog
						title="Enter remark"
						actions={this.actions}
						model={true}
						autoScrollBodyContent={true}
						open={this.state.showRemark}
						autoLockScrolling={true} >
						<div className="row">
							<div className="col-sm-12">
								<TextField
									fullWidth={true}
									floatingLabelText={"Remark"}
									multiLine={true}
									rows={2}
									errorText={this.state.errorText}
									defaultValue={this.state.remark}
									value={this.state.remark}
									onChange={this.updateRemark}
								/>
							</div>
						</div>
					</Dialog>
					<Link to={`/crm/opportunity/${companyID}/${opportunity.id}`} >{displayChance}, {numeral(opportunity.packagePrice).format('$0,0.00')}</Link>
				</div>
			)
		}
	}
}
