import React from 'react'
import SelectField from 'material-ui/SelectField';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import {Map} from 'immutable';
import { bindActionCreators } from 'redux'
import Opportunity from '../opportunity'

import * as OpportunityAction from '../opportunity/action'

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
const stage = require('../../share/stage.json'),
			_ = require('lodash'),
			moment = require('moment')

const StageMenuItems = (list)=> {
  return list.map(function(k) {
    return (
      <MenuItem value={k.id} key={k.id} primaryText={k.value} style={{"font-size":"12px", "color":"#0a6ebd"}}/>
    )
  })
}


const FUPForm = (updateChange, errorFields) => {
	return ()=> {
		return (
			<div className="row align-items-center">
				<div className="col-12">
					<DatePicker
						hintText="Follow Up Date"
						floatingLabelText="Follow Up Date"
						fullWidth={true}
						formatDate={function(date) {
							return moment(date).format('DD/MM/YYYY')
						}}
						errorText={errorFields.followUpDate}
						autoOk={true}
						minDate={new Date()}
						onChange={(i,v)=>{
							updateChange(v)
						}}
					/>
				</div>
			</div>
		)
	}
}


export default class EditStage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			editing: false,
			openFUP: false,
			openOpporunity: false,
			errorFUP: {}
		}
		this.onClickHandler = this.onClickHandler.bind(this)
		this.onChangeHandler = this.onChangeHandler.bind(this)
		this.addChangedData = this.addChangedData.bind(this)
		this.saveFUPDate = this.saveFUPDate.bind(this)
		this.afterOpportunitySave = this.afterOpportunitySave.bind(this)
		this.validateField = this.validateField.bind(this)
		this.resetValue = this.resetValue.bind(this)
		this.actions = [
      <FlatButton
        label="Save"
        primary={true}
        onClick={this.saveFUPDate}
      />
		];
		this.changedData = {}
    this.customer = this.props.customer    
		this.opportunityAction = bindActionCreators(OpportunityAction, this.props.dispatch)
	}

	afterOpportunitySave(value) {
		this.customer.latestOpportunityID = value.id
		this.customer.latestOpportunity = value
		this.saveFUPDate()
	}

	resetValue() {
		this.customer = this.props.customer    
		this.setState((prevState)=> {
			return _.extend({}, prevState, {openFUP: false, openOpporunity: false, editing: false})
		})
	}


	saveFUPDate() {
		this.setState((prevState)=> {
			return _.extend({}, prevState, {openFUP: false, openOpporunity: false, editing: false})
		})		
		this.props.changeStage(this.customer, this.customer.id)
	}

	addChangedData(FUP) {
		this.customer.followUpDate = FUP
	}

	onChangeHandler(e,i,v) {
		e.preventDefault()
		var self = this		
		this.setState((prevState)=> {
			self.customer.pitchStatusID = v
			if(v == 17 || v == 18) {
				if(v != self.props.value) {
					self.props.editingCompanyStage(v, self.customer.id)
					return _.extend({}, prevState, {openFUP: false, openOpporunity: true, editing: false})
				}
			}
			self.props.editingCompanyStage(v, self.customer.id)
			self.props.openFUPDate(self.customer.id)()
			return _.extend({}, prevState, {openFUP: true, openOpporunity: false, editing: false})
		})

	}

	onClickHandler(e) {
		e.preventDefault()
		this.setState((prevState)=> {
			return _.extend({}, prevState, {editing: true})
		})
		return false;
	}

	validateField(errorField) {
		this.setState((prevState)=> {
			return _.extend({}, prevState, {errorFUP: {}})
		})		
		if(_.isEmpty(this.customer.followUpDate.toString())) {			
			errorField['followUpDate'] = "Cannot be empty"
			this.setState((prevState)=> {
				return _.extend({}, prevState, {errorFUP: errorField})
			})		
		}
		return errorField
	}

	render() {
		var {editing, openFUP, openOpporunity, errorFUP} = this.state
		var value = this.props.value
		if(value<=0 || !value) {
			value = 15
		}
		var displayText = "Loading"
		if(this.props.refList.has('stage')) {
			this.props.refList.get('stage').map(function(v) {
				if(v.id == value) {
					displayText = v.value
				}
			})
		}

		if(openOpporunity) {
			this.opportunityAction.toggleDialog(true)
			return <Opportunity
				needFollowUpDate={FUPForm(this.addChangedData, errorFUP)} 
				afterSave={this.afterOpportunitySave}
				sendCompany={this.customer}
				cancelSave={this.resetValue}
				validateField={this.validateField}
			/>
		}

		if(this.props.refList.has('stage')) {
			return (
				<SelectField onChange={this.onChangeHandler} value={value}
				style={{"font-size":"12px" }}
				labelStyle={{"color":"#0a6ebd"}}
				>
					{StageMenuItems(this.props.refList.get('stage'))}
				</SelectField>
			)
		} else {
			return null
		}
	}
}
