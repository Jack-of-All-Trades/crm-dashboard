// @flow
import React from 'react'
import ReactDOM from 'react-dom'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { Link } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import IconButton from 'material-ui/IconButton';
import {TextField} from 'material-ui'
import * as DashBoardActions  from './actions'
import * as CustomerActions from '../customer/action'
import * as OpportunityActions from '../opportunity/action'
import * as UserActions  from '../../../base/components/User/actions'
import ContentAdd from 'material-ui/svg-icons/content/add';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import DateRange from 'material-ui/svg-icons/action/date-range'
import LatestConversation from './lastConversation'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import CreateIcon from 'material-ui/svg-icons/content/create'
import {Map} from 'immutable';
import DisplayOwner from './displayOwner'
import * as Scroll from 'react-scroll';

import { BootstrapTable, TableHeaderColumn, SearchField } from 'react-bootstrap-table';
import EditStage from './editStage'
import EditChance from './editChance'
import FUPDate from './FUPDate'

import ReportDisplay from './reportDisplay'


import * as gridStyle from './grid.less'

import OverDue from '../customer/overdue'

var moment = require('moment'),
    chance = require('../../share/chance.json'),
    stage = require('../../share/stage.json'),
    type = require('../../share/type.json'),
    year = require('../../share/financialYear.json'),
    operation =require("../../share/operation.json"),
    lead = require('../../share/lead.json')

function mapStateToProps(state) {
  return {
    data: state.crm_dashboard.get('data'),
    editingStage: state.crm_dashboard.get('editingStage'),
    referenceConfig: state.crm_dashboard.get('referenceConfig'),
    gridSearch: state.crm_dashboard.get('gridSearch'),
    totalCount: state.crm_dashboard.get('totalCount'),
    needOpportunity: state.crm_customer.get('needOpportunity'),
    users: state.user.get('users'),
    allUsers: state.user.get("allUsers"),
    logonUser: state.Auth.get('userProfile'),
    stats: state.crm_dashboard.get('stats')
  };
}

class MySearchField extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      searchText: this.props.defaultValue? this.props.defaultValue : ""
    };
    this.changeSearchValue = this.changeSearchValue.bind(this)
    
  }

  // It's necessary to implement getValue
  getValue() {
    return this.state.searchText
  }

  // It's necessary to implement setValue
  setValue(value) {
    ReactDOM.findDOMNode(this).value = value;
  }

  changeSearchValue(obj, value) {
    this.setState((prevState, props) => {
      return {searchText: value};
    });
  }

  render() {
    return (
      <TextField
        fullWidth={true}
        value={this.state.searchText}
        defaultValue={this.props.defaultValue}
        floatingLabelText={this.props.placeholder}
        floatingLabelFixed={true}
        onKeyUp={this.props.search}
        onChange={this.changeSearchValue}
      />
    );
  }
}

const createCustomSearchField = (props) => {
  return (
    <SearchField
        className='my-custom-class'
        placeholder='Input a number'/>
  );
}
const CreateCustomToolBar = props => {
  return (
    <div className='col-12'>
      { props.components.searchPanel }
    </div>
  );
}

const Company = (name, dependentValues) => {
  var strikeClass = {}
  return (
    <Link to={`/crm/company/${dependentValues.id}`} >{dependentValues.name}</Link>
  )
}

const StageField = (value, all, editingStage, onClick, changeStage) => {
  var currentState = editingStage.get(all.latestOpportunity.id, false)
  const click = () => {
    onClick(currentState, all.latestOpportunity.id)
  }
  if(!currentState) {
    return (
      <a href="#" onClick={click}>
        {value}
      </a>
    )
  } else {
    var key = _.findKey(stage, function(o) { return o == value; });
    return (
      <SelectField floatingLabelText="Stage" fullWidth={true} onChange={(e,i,v)=> {
        changeStage(v, all, all.latestOpportunity)
      }} value={key}>
        {StageMenuItems()}
      </SelectField>
    )
  }
}

const Contact = (name, dependentValues) => {
  var contact = dependentValues.contacts.map(function(c) {
    if(c.statusID == 20) {
      return (
        <div className={gridStyle.contactContainer}>
          {c.name} ({c.title}) <br />
          {c.contact}, <a href={`mailto:"${c.email}"`}>{c.email}</a>
        </div>
      )
    }
  })
  return (
    <div>
      {contact}
    </div>
  )
}

const DisplayType = ({value}) => {
  if(value.length<=0)
    return null

  var displayType = []
  _.forEach(value, function(v) {
    displayType.push(v.value)
  })
  return (
    <div dangerouslySetInnerHTML={{__html: displayType.join("<br />")}}>
    </div>
  )
}

@withRouter
@connect(mapStateToProps)
export default class DashBoard extends React.Component {
  constructor(props) {
    super(props)
    this.dispatch = props.dispatch
    this.actions = bindActionCreators(DashBoardActions, props.dispatch)
    this.customerActions = bindActionCreators(CustomerActions, props.dispatch)
    this.opportunityActions = bindActionCreators(OpportunityActions, props.dispatch)
    this.userActions = bindActionCreators(UserActions, props.dispatch)
    this._rows = []
    this.updateCustomer = this.updateCustomer.bind(this)
    this.handleSortOrderChange = this.handleSortOrderChange.bind(this)
    this.onPageChangeHandler = this.onPageChangeHandler.bind(this)    
    this.onRowSizeChange = this.onRowSizeChange.bind(this)
    this.searchChange = this.searchChange.bind(this)
    this.refreshGrid = this.refreshGrid.bind(this)
    this.editingCompanyStage = this.editingCompanyStage.bind(this)
    this.setFUPDateRef = this.setFUPDateRef.bind(this)
    this.openFUPDate = this.openFUPDate.bind(this)
    this.loadStats = this.loadStats.bind(this)
    this.editingCompany = {}
    this.FUPDateRef = Map()
    this.scroll = Scroll.animateScroll;
    this.changeOtherUserRowColor = this.changeOtherUserRowColor.bind(this)
    this.logonUser = props.logonUser
    this.isAdmin = false
    var roles = this.logonUser.get('roles')
    var self = this
    _.forEach(roles, function(role) {
      if(role.moduleName == "crm" && role.name == "admin")
        self.isAdmin = true
    })
    this.state = {
      reportSelectedValue: this.isAdmin?0:props.logonUser.get('id')
    }
  }

  editingCompanyStage(value, companyID) {
    this.editingCompany = {
      pitchStatusID: value,
      id: companyID
    }
  }

  setFUPDateRef(ref, id) {

    this.FUPDateRef = this.FUPDateRef.set(id, ref)
  }

  openFUPDate(id) {
    var self = this
    return ()=>{
      if(self.FUPDateRef.has(id)) {
        var ref = self.FUPDateRef.get(id)
        ref.openDialog()
      }
    }

  }

  updateCustomer(value, companyID, afterSave) {
    value.id = companyID
    var self = this
    var updatingData = {}
    if(companyID == this.editingCompany.id) {
      updatingData = _.merge(this.editingCompany, value)
    } else {
      updatingData = value
    }
    this.customerActions.saveCustomer(updatingData,[], function(newData) {
      let {gridSearch} = self.props
      self.actions.loadList(gridSearch)
      if(_.isFunction(afterSave)) {
        afterSave()
      }
    })
  }

  refreshGrid() {
    let {gridSearch} = this.props
    this.actions.loadList(gridSearch)
  }

  onPageChangeHandler(page) {
    let {gridSearch} = this.props
    gridSearch.page = page
    if(gridSearch.page<=0) {
      gridSearch.page = 1
    }
    this.actions.loadList(gridSearch)
    this.scrollTop()
  }

  scrollTop() {
    this.scroll.scrollTo("topOfGrid",{
      smooth: true,
      duration: 500
    })
  }

  handleSortOrderChange(col,direction) {
    let {gridSearch} = this.props
    gridSearch.sort = col+' '+_.toUpper(direction)
    this.actions.loadList(gridSearch)
    this.scrollTop()
  }
  onRowSizeChange(limit) {
    let {gridSearch, totalCount} = this.props
    if(limit == this.props.totalCount) {
      gridSearch.limit = limit
      gridSearch.page = 1
    }
    else 
      gridSearch.limit = limit
    this.actions.loadList(gridSearch)
    this.scrollTop()
  }

  loadStats(userID) {
    let {gridSearch} = this.props
    this.setState((prevState)=> {
      return _.extend({}, prevState, {reportSelectedValue: userID})
    })
    this.actions.getStats(userID)
    gridSearch.userID = userID
    this.actions.loadList(gridSearch)
  }

  componentDidMount() {
    let {gridSearch, logonUser} = this.props
    this.actions.loadList(gridSearch)
    this.loadStats(this.state.reportSelectedValue)
    this.customerActions.getNeedOpportunity()
    this.actions.loadAllReference()
    
    this.userActions.getAllUser("crm")
  }
  remote(remoteObj) {
    // Only cell editing, insert and delete row will be handled by remote store
    remoteObj.sort = true;
    remoteObj.pagination = true
    remoteObj.search = true
    return remoteObj;
  }

  searchChange = (searchText, colInfos, multiColumnSearch) => {
    let {gridSearch} = this.props
    if(gridSearch.search != searchText) {
      gridSearch.search = searchText
      gridSearch.page = 1
      if(!_.isEmpty(gridSearch.search)) {
        gridSearch.userID = 0
      } else {
        gridSearch.userID = this.state.reportSelectedValue
      }
      this.actions.loadList(gridSearch)
      this.scrollTop()
    }
  }

  changeOtherUserRowColor(fieldValue, row, rowIdx, colIdx) {
    // fieldValue is column value
    // row is whole row object
    // rowIdx is index of row
    // colIdx is index of column
    var returnClass = []
    if(row.ownerID != this.logonUser.get('id')) {
      returnClass.push(gridStyle.otherUser)
    }
    if(row.operationID != 1) {
      returnClass.push(gridStyle.notOperating)
    }
    return returnClass
  }

  render() {
    let { data, gridSearch, totalCount, needOpportunity, referenceConfig, users, logonUser, stats, allUsers } = this.props
    if(needOpportunity.length>0) {
      this.customerActions.removeNeedOpportunity()
      this.props.history.push('/crm/opportunity/?customerID='+needOpportunity[0].id+"&forceOpen=1")
    }
    let { newCustomer } = this.customerActions
    let { reportSelectedValue } = this.state
    const options = {
      ignoreEditable: true,
      onSortChange: this.handleSortOrderChange,
      onPageChange: this.onPageChangeHandler,
      sizePerPageList: [ 30, 50, 100, {
        text:"All",
        value: this.props.totalCount
      } ],
      sizePerPage: gridSearch.limit,
      page: gridSearch.page,
      onSearchChange: this.searchChange,
      searchDelayTime: 800,
      onSizePerPageList: this.onRowSizeChange,
      searchField: (props) => (<MySearchField { ...props }/>),
      toolBar: CreateCustomToolBar
    };
    return (
      <Scroll.Element name="topOfGrid">
        <OverDue refreshGrid={this.refreshGrid} />
        
        <div className="col-md-12">
          <article className="article">
            <h2 className="article-title">Dashboard</h2>
            <RaisedButton onClick={()=>{
              newCustomer()
            }} containerElement={<Link to="/crm/company" />} labelPosition="after" icon={<ContentAdd />} primary={true} label="Company" />{" "}
            <RaisedButton containerElement={<Link to="/crm/report" replace  />} labelPosition="after" icon={<ActionAssignment />} primary={true} label="Report" />{" "}
              <div className={gridStyle.reportContainer + " row justify-content-center align-items-center"}>
                <div className="col-md-8 col-sm-12">
                  <ReportDisplay stats={stats} allUsers={allUsers} logonUser={logonUser} loadStats={this.loadStats} reportSelectedValue={reportSelectedValue} />
                </div>
              </div>
            <BootstrapTable
              version='4'
              data={data.toArray()}
              keyField="id"
              remote={this.remote}
              search
              options={options}
              searchPlaceholder="Search (type in company, email, name, contact number)"
              pagination
              tableContainerClass={gridStyle['smallContainer']}
              fetchInfo={ { dataTotalSize: this.props.totalCount }}
            >
              <TableHeaderColumn width='15%' columnClassName={ this.changeOtherUserRowColor } dataField="name"  editable={false} dataFormat={Company} dataSort={true}>Company Name</TableHeaderColumn>
              <TableHeaderColumn width='20%' columnClassName={ this.changeOtherUserRowColor } dataField="contacts"  editable={false} dataFormat={Contact} dataSort={true}>Contacts</TableHeaderColumn>
              <TableHeaderColumn width='7%' columnClassName={ this.changeOtherUserRowColor } dataField="pitchStatusID" editable={ false } dataSort={true} dataFormat={(value,all)=> {
                if(all.operationID ==2)
                  return null
                return <EditStage openFUPDate={this.openFUPDate} editingCompanyStage={this.editingCompanyStage} changeStage={this.updateCustomer} refList={referenceConfig} customer={all} value={value} dispatch={this.props.dispatch}/>
              }}>Stage</TableHeaderColumn>
              <TableHeaderColumn width='8%' columnClassName={ this.changeOtherUserRowColor } dataField="followUpDate" editable={false} dataSort={true} dataFormat={(value,all)=> {
                if(all.operationID ==2)
                  return null
                return <FUPDate setFUPDateRef={this.setFUPDateRef} openFUPDate={this.openFUPDate} updateData={this.updateCustomer} all={all} value={value}/>
              }}>FUP Date</TableHeaderColumn>
              <TableHeaderColumn width='20%' columnClassName={ this.changeOtherUserRowColor } dataField="conversation" editable={false} dataFormat={(value, all) => {
                if(all.operationID ==2)
                  return null
                return <LatestConversation refreshGrid={this.refreshGrid} companyID={all.id} communication={all.latestConversation} all={all} />
              }}>Conversation</TableHeaderColumn>
              <TableHeaderColumn width='11%' columnClassName={ this.changeOtherUserRowColor } dataField="chance" editable={false} dataSort={true} dataFormat={(value, all) => {
                if(all.operationID ==2)
                  return null
                return <EditChance refreshGrid={this.refreshGrid}  refList={referenceConfig} dispatch={this.props.dispatch} companyID={all.id} opportunity={all.latestOpportunity} all={all} />
              }}>Chance</TableHeaderColumn>
              <TableHeaderColumn width='11%' columnClassName={ this.changeOtherUserRowColor } dataField="companyType" editable={false} dataFormat={(value, all)=><DisplayType value={value} />}>Type</TableHeaderColumn>
              <TableHeaderColumn width='8%' columnClassName={ this.changeOtherUserRowColor } dataField="ownerID" editable={false} dataFormat={(value, all)=><DisplayOwner updateCompany={this.updateCustomer} dispatch={this.props.dispatch} allUsers={allUsers} value={value} company={all} />}>Owner</TableHeaderColumn>
            </BootstrapTable>
          </article>
        </div>
      </Scroll.Element>
    );
  }
}
