export default (value, all) => {
	var currentState = editingStage.get(all.latestOpportunity._id, false)
  const click = () => {    
    onClick(currentState, all.latestOpportunity._id)
  }
  if(!currentState) {
    return (
      <a href="#" onClick={click}>
        {value}
      </a>
    )
  } else {    
    var key = _.findKey(stage, function(o) { return o == value; });
    return (
      <SelectField floatingLabelText="Stage" fullWidth={true}  onChange={(e,i,v)=> {
        onClick(currentState, all.latestOpportunity._id)
        changeStage(v, all, all.latestOpportunity)
      }} value={key}>
        {StageMenuItems()}
      </SelectField> 
    )
  }
}