import React from 'react'
import * as gridStyle from './grid.less'

export default (props)=> {
  
  if(props.value) {
    return value.map((_value)=> {
      var title = ""
      if(_value.title) {
        title = "("+_value.title+")"
      }
      var contact = ""
      if(_value.contact) {
        contact = _value.contact+", "
      }
      return (
        <div className={gridStyle.contactContainer}>
          {_value.name} {title} <br />
          {contact}{_value.email}
        </div>
      )
    })
  }
  return null;
}
