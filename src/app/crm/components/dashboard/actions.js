import * as DashboardConstant from './constants'
import axios from '../../services/request'
import {Map, List} from 'immutable'
import parallelLimit from 'async/parallelLimit';

const moment = require('moment')


const querystring = require('querystring')

export function loadList(gridSearch) {
  return (dispatch) => {
    dispatch({
      type: DashboardConstant.LOADING_GRID,
      searchObj:gridSearch
    });
    axios.get('/crm/company', {params: gridSearch})
      .then((data)=>{
        dispatch({
          type: DashboardConstant.LOADED_GRID,
          data: data.data,
          totalCount: Number(data.headers['x-total-length'])
        })
        return {data: data.data, totalCount: data.headers['x-total-length']}
      })
  }
}

export function getStats(userID) {
  return (dispatch) => {
    dispatch({
      type: DashboardConstant.LOADING_STATS
    });    
    var requestStats = {
      "today": function(cb) {
        axios.get('/crm/company', {params: {count: 1, userID:userID, startFupDate: moment().toDate(), endFupDate: moment().toDate()}})
        .then((data)=>{
          cb(null,data.data)          
        })
      },
      "sevenDays": function(cb) {
        axios.get('/crm/company', {params: {count: 1, userID:userID, startFupDate: moment().add(1, 'days').toDate(), endFupDate: moment().add(7,"days").toDate()}})
        .then((data)=>{
          cb(null,data.data)          
        })
      },
      status:function(cb) {
        axios.get('/crm/company', {params: {statsPitchStatus:1, userID:userID}})
        .then((data)=>{
          var returnValue = {
            new: 0,
            prospecting:0,
            proposed: 0,
            won: 0,
            notKeen: 0
          }
          if(data.data && data.data.length>0) {
            for(var o=0;data.data.length>o;o++) {
              var currentV = data.data[o]
              switch(currentV.pitchStatusID) {                
                case 16: returnValue.prospecting = currentV.statusCount
                  break;
                case 17: returnValue.proposed = currentV.statusCount
                  break;
                case 18: returnValue.won = currentV.statusCount
                  break;
                case 19: returnValue.notKeen = currentV.statusCount
                  break;
                default: returnValue.new = currentV.statusCount
                  break;
              }
            }
          }
          cb(null,returnValue)          
        })
      },
      pipelineWon:function(cb) {
        axios.get('/crm/opportunity', {params: {"chanceID": 0, userID:userID}})
        .then((data)=>{
          var returnValue = 0
          if(data.data && data.data.length>0) {
            returnValue = data.data[0].totalAmt
          }
          if(returnValue<=0)
            returnValue = "0"
          cb(null,returnValue)    
        })
      },
      pipelineChance:function(cb) {
        axios.get('/crm/opportunity', {params: {"chanceGroup": 1, userID:userID}})
        .then((data)=>{
          var returnValue = 0
          if(data.data && data.data.length>0) {
            for(var o=0;data.data.length>o;o++) {
              var currentV = data.data[o]
              switch(currentV.chanceID) {
                case 25: returnValue += (currentV.totalAmt * 0.25)
                  break;
                case 26: returnValue += (currentV.totalAmt * 0.5)
                  break;
                case 27: returnValue += (currentV.totalAmt * 0.75)
                  break;
                case 28: returnValue += (currentV.totalAmt * 1)
                  break;
                default: returnValue += (currentV.totalAmt * 0)
                  break;
              }
            }
          }
          if(returnValue<=0)
            returnValue = "0"
          cb(null,returnValue)         
        })
      },
      thisMonth:function(cb) {
        axios.get('/crm/conversation/Company', {params: {count:1,userID: userID, endDate: moment().toDate(), startDate: moment().startOf('month').toDate()}})
        .then((data)=>{
          cb(null,data.data)          
        })
      },
      lastMonth:function(cb) {
        var lastMonth = moment().subtract(1, 'months')
        axios.get('/crm/conversation/Company', {params: {count:1, userID: userID, endDate: lastMonth.endOf('month').toDate(), startDate: lastMonth.startOf('month').toDate()}})
        .then((data)=>{
          cb(null,data.data)          
        })
      },
      thisMonthSales:function(cb) {
        axios.get('/crm/opportunity', {params: {chanceID: 28, statsPitchStatus: 1, userID:userID, endCampaignStartDate: moment().toDate(), startCampaignStartDate: moment().startOf('month').toDate()}})
        .then((data)=>{
          var returnValue = 0
          if(data.data && data.data.length>0) {
            returnValue = data.data[0].totalAmt
          }
          if(returnValue<=0)
            returnValue = "0"
          cb(null,returnValue)          
        })
      },
      lastMonthSales:function(cb) {
        var lastMonth = moment().subtract(1, 'months')
        axios.get('/crm/opportunity', {params: {chanceID: 28, statsPitchStatus: 1, userID:userID, endCampaignStartDate: lastMonth.endOf('month').toDate(), startCampaignStartDate: lastMonth.startOf('month').toDate()}})
        .then((data)=>{
          var returnValue = 0
          if(data.data && data.data.length>0) {
            returnValue = data.data[0].totalAmt
          }
          if(returnValue<=0)
            returnValue = "0"
          cb(null,returnValue)        
        })
      }
    }
    parallelLimit(requestStats, 2, function(err,d){
      dispatch({
        type: DashboardConstant.LOADED_STATS,
        data: d
      });
    })
    
  }
}

export const loadAllReference = ()=>{
  return (dispatch) => {
    dispatch({
      type: DashboardConstant.LOADING_REFERENCE
    });
    axios.get('/crm/reference')
      .then((data)=>{
        dispatch({
          type: DashboardConstant.DONE_REFERENCE,
          reference: data.data
        })
      })
  }
}


export const gridCustomer = ({sort="", page=1, search="", limit=15}) => {


}

export const updateGridCompanyOpportunity = (companyID, opportunity)=>{
  return (dispatch) => {
    dispatch({
      type: DashboardConstant.UPDATE_COMPANY_OPPORTUNITY,
      opportunity: opportunity,
      companyID: companyID
    })
  }
}

export function toggleStage(current, id) {
  return (dispatch) => {
    if(current) {
      dispatch({
        type: DashboardConstant.GRID_EDIT_STAGE_STOP,
        oppID: id
      })
    } else {
      dispatch({
        type: DashboardConstant.GRID_EDIT_STAGE,
        oppID: id
      })
    }
  }

}
