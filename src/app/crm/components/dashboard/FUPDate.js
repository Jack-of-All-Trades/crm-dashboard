import React from 'react'
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import DatePicker from 'material-ui/DatePicker';

const moment = require('moment')
const FUPDate = ({value, all, updateData, setFUPDateRef, openFUPDate}) => {
  var display = ""
  if(!_.isEmpty(value)) {
    display = moment(value).toDate()
  }  
  return (
    <div>
      <DatePicker
        id="changeDate"
        onChange={(i,v)=>{
          updateData({followUpDate: v}, all.id)
        }}
        formatDate={function(date) {
          return moment(date).format('DD MMM')
        }}
        hintText="Add"
        defaultDate={display}
        value={display}
        fullWidth={true}
        inputStyle={{"font-size":"12px", "color":"#0a6ebd"}}
        autoOk={true}
        ref={(ref)=>setFUPDateRef(ref, all.id)} />
    </div>
  )
}

export default FUPDate