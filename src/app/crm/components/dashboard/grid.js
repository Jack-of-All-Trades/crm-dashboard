import React from 'react'
import {Card, CardActions, CardText, CardTitle} from 'material-ui/Card'
import TextField from 'material-ui/TextField';
import classNames from 'classnames'
import { Link } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Owner from '../opportunity/owner'
import CustomerType from '../customer/customerType'

var ReactDataGrid = require('react-data-grid/addons');

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';


import {Map, List} from 'immutable'

import gridStyle from './grid.less'

var moment = require('moment'),
    chance = require('../../share/chance.json'),
    stage = require('../../share/stage.json'),
    type = require('../../share/type.json'),
    year = require('../../share/financialYear.json'),
    operation =require("../../share/operation.json"),
    lead = require('../../share/lead.json')


function contactData(values) {
  return values.map((value)=> {
    return (
      <TableRow key={value._id}>
        <TableRowColumn className={gridStyle.midColumn}>{value.name}</TableRowColumn>
        <TableRowColumn className={gridStyle.xsmallColumn}>{value.title}</TableRowColumn>
        <TableRowColumn className={gridStyle.emailCol}><a target="_blank" href={`mailto:${value.email}`}>{value.email}</a></TableRowColumn>
        <TableRowColumn className={gridStyle.midColumn}>{value.contact}</TableRowColumn>
      </TableRow>
    )
  })
}

const ContactsTable = ({data})=> {
  if(data==null) {
    return null
  }
  return (
    <Table selectable={false}>
      <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {contactData(data)}
      </TableBody>
    </Table>
  )
}

const CheckOpportunity = ({children, opportunity})=> {    
  if(!_.isEmpty(opportunity)) {
    return children
  }
  return null
}

function gridData(data, loadedCustomer, loadCustomer) {
  return data.map((value,key)=>{    
    var conversation="Add new";
    if(!_.isEmpty(value.lastConversation)) {
      conversation = value.lastConversation;
    }
    var latestOpportunity = {};
    if(value.opportunities.length>0) {
      var startDate = null;
      _.forEach(value.opportunities, function(v) {
        if(startDate == null) {
          startDate = moment(v.campaignStartDate)
        }
        var currentStartDate = moment(v.campaignStartDate)
        if(startDate.isSameOrBefore(currentStartDate)) {
          startDate = currentStartDate
          latestOpportunity = v
        }
      })

    }
    return (
      <TableRow key={value._id}>
        <TableRowColumn className={gridStyle.midColumn} ><Link to={`/crm/company/${value._id}`} >{value.name}</Link></TableRowColumn>
        <TableRowColumn className={gridStyle.contactColumn}><ContactsTable data={value.contacts} /></TableRowColumn>
        <TableRowColumn className={gridStyle.midColumn}>
          <CheckOpportunity opportunity={latestOpportunity}>
            <Link to={`/crm/opportunity/${latestOpportunity._id}`} >{stage[latestOpportunity.stage]}</Link>
          </CheckOpportunity>
        </TableRowColumn>

        <TableRowColumn className={gridStyle.smallColumn}>
          <CheckOpportunity opportunity={latestOpportunity}>
            <div>
              {moment(latestOpportunity.followUpDate).format('DD MMM')}
            </div>
          </CheckOpportunity>
        </TableRowColumn>
        <TableRowColumn className={gridStyle.conversationColumn}>
          <CheckOpportunity opportunity={latestOpportunity}>
            <Link to={`/crm/communication/${latestOpportunity._id}`} >{conversation}</Link>
          </CheckOpportunity>
        </TableRowColumn>
        <TableRowColumn className={gridStyle.smallColumn}>
          <CheckOpportunity opportunity={latestOpportunity}>
            <div>{chance[latestOpportunity.chance]}</div>
          </CheckOpportunity>
        </TableRowColumn>
        <TableRowColumn className={gridStyle.smallColumn}>
          <CustomerType companyType={value.type} />          
        </TableRowColumn>
        <TableRowColumn className={gridStyle.smallColumn}>
          <Owner ownerID={value.ownerID} />       
        </TableRowColumn>
      </TableRow>
    )
  })
}
export default function Grid({data=List(), loadedCustomer=Map(), loadCustomer, newCustomer}) {
	return (
		<div className="col-md-12">
      <Table selectable={false} bodyStyle={{overflow:'auto'}} wrapperStyle={{overflowX:"hidden"}}>
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
              <TableRow>
                <TableHeaderColumn className={gridStyle.midColumn}>Company</TableHeaderColumn>
                <TableHeaderColumn className={gridStyle.contactColumn}>Contacts</TableHeaderColumn>
                <TableHeaderColumn className={gridStyle.midColumn}>Stage</TableHeaderColumn>
                <TableHeaderColumn className={gridStyle.smallColumn}>FUP Date</TableHeaderColumn>
                <TableHeaderColumn className={gridStyle.conversationColumn}>Conversation</TableHeaderColumn>
                <TableHeaderColumn className={gridStyle.smallColumn}>Chance</TableHeaderColumn>
                <TableHeaderColumn className={gridStyle.smallColumn}>Type</TableHeaderColumn>
                <TableHeaderColumn className={gridStyle.smallColumn}>Owner</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {gridData(data, loadedCustomer, loadCustomer)}
            </TableBody>
          </Table>
    </div>
	)
}
