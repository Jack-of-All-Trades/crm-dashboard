import * as DashBoardConstants from './constants'
import {Map, List} from 'immutable'
var moment = require('moment')

const initialState = Map({
  data: List(),
  loadedCustomer: Map(),
  isLoadingData: false,
  loadingCustomer: false,
  editingStage: Map(),
  gridSearch: {sort:"", page:1, search:"", limit:50},
  totalCount: 0,
  referenceConfig: Map(),
  stats: {}
})

export default (state=initialState, action) => {  
  let reducer = {}
  reducer[DashBoardConstants.LOADING_GRID] = ({searchObj})=> {    
    return state.set('isLoadingData', true).set('gridSearch',searchObj)
  }
  reducer[DashBoardConstants.LOADED_GRID] = ({data, totalCount})=> {
    return state.set('data',List(data)).set('isLoadingData', false).set('totalCount', totalCount)
  }
  reducer[DashBoardConstants.GRID_EDIT_STAGE] = ({oppID})=> {   
    var stage = state.get('editingStage')
    stage = stage.set(oppID, true)     
    return state.set('editingStage',stage)
  }
  reducer[DashBoardConstants.GRID_EDIT_STAGE_STOP] = ({oppID})=> {
    var stage = state.get('editingStage')
    stage = stage.set(oppID, false)     
    return state.set('editingStage',stage)
  }
  reducer[DashBoardConstants.LOADING_REFERENCE] = () => {
    return state.set('referenceConfig',Map())    
  }

  reducer[DashBoardConstants.LOADING_STATS] = () => {
    return state.set('stats',{})    
  }

  reducer[DashBoardConstants.LOADED_STATS] = ({data}) => {
    return state.set('stats',data)    
  }


  reducer[DashBoardConstants.DONE_REFERENCE] = ({reference}) => {
    var newMap = Map()
    reference.map((v) => {
      if(!newMap.has(v.reference)) {
        newMap = newMap.set(v.reference, [])
      }
      var currentMap = newMap.get(v.reference)
      currentMap.push(v)
      newMap = newMap.set(v.reference, currentMap)
    })
    return state.set('referenceConfig',newMap)    
  }
  reducer[DashBoardConstants.UPDATE_COMPANY_OPPORTUNITY] = ({companyID, opportunity}) => {
    var loadedData = state.get('data')
    for(var i=0;i<loadedData.length;i++) {
      var v = loadedData.get(i)
      if(v._id == companyID && v.latestOpportunity._id == opportunity._id) {
        v.latestOpportunity = opportunity
        loadedData.set(i, v)
      }
    }
    return state.set('data',loadedData)
  }
  reducer['default'] = ()=> {
    return state
  }
  return (reducer[action.type] || reducer['default'])(action)
}