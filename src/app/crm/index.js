import React from 'react';
import {
  BrowserRouter,
  Route,
  Redirect,
  Link
} from 'react-router-dom';
import App from './components/dashboard'
import Customer from './components/customer'
import Opportunity from './components/opportunity'
import Communication from './components/communication'
import Report from './components/report';
import PatternReport from './components/report/pattern'
import FlatButton from 'material-ui/FlatButton';

import dashboard from './components/dashboard/reducer';
import customer from './components/customer/reducer';
import opportunity from './components/opportunity/reducer';
import communication from './components/communication/reducer'
import report from './components/report/reducer'
import PrivateRoute from '../base/components/Login/privateRoute'

import DocumentTitle from "react-document-title";

import axios from './services/request'

export const Reducer = {
  "crm_dashboard":dashboard,
  "crm_customer":customer,
  "crm_opportunity":opportunity,
  "crm_communication":communication,
  "crm_report":report
}

export const Nav = ()=> {
	return [
		<li key="dashboard" style={{paddingTop:"10px"}} className="list-inline-item">
      <FlatButton containerElement={<Link to="/crm" />} >
        <span className="nav-text">CRM</span>
      </FlatButton>
    </li>
	]
}

export const Render = ({token}) => {
  axios.defaults.headers.common['Authorization'] = "Bearer "+token;
  document.title="CRM"
	return (
      <div id="crm-module">
        <PrivateRoute path="/crm" component={App} isNew={true} exact />
        <PrivateRoute path="/crm/report" component={Report} exact />
        <PrivateRoute path="/crm/report/pattern" component={PatternReport} exact />
        <Route path="/crm/company" component={Customer} isNew={true}/>
        <Route path="/crm/company/:customerID" component={Customer} isNew={false} />
        <Route path="/crm/opportunity/:customerID" component={Opportunity} isNew={false} exact={true} />
        <Route path="/crm/opportunity/:customerID/:id" component={Opportunity} isNew={false}  exact={true} />
        <Route path="/crm/opportunity" component={Opportunity} isNew={true} />
        <Route path="/crm/communication/:companyID" component={Communication} />
      </div>)
}
