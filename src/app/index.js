import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { BrowserRouter } from 'react-router-dom'
import { MuiThemeProvider as MuiThemeProviderNew, createMuiTheme } from '@material-ui/core/styles';

// = styles =
// 3rd
import './base/vendors/bootstrap.scss';
// custom
import './base/vendors/layout.scss';
import './base/vendors/theme.scss';
import './base/vendors/ui.scss';
import './base/vendors/app.scss';

import lightTheme from './base/styles/lightTheme';


import App from './base'
import * as CRM from './crm'
import * as Novaly from './novaly'

const theme = createMuiTheme();

const mapStateToProps = (state, ownProp) => {      
  return {    
    token:state.Auth.get('token',null)
  }
}

export function MainBody(props) {  
  return (
    <MuiThemeProviderNew theme={theme}> 
      <MuiThemeProvider muiTheme={getMuiTheme(lightTheme)}>
        <div id="app-inner">
          <div className="preloaderbar hide"><span className="bar" /></div>
          <div
            className={classnames('full-height', {
              'fixed-header': true,
              'nav-collapsed': true,
              'nav-behind': false,
              'layout-boxed': false,
              'sidebar-sm': true
              })}>
            <App>
              <CRM.Render nav={CRM.Nav} token={props.token} />
              <Novaly.Render token={props.token} />
            </App>
          </div>
        </div>
      </MuiThemeProvider>
    </MuiThemeProviderNew>
  )
}


class AppHandler extends Component {
  constructor(props) {
    super(props)    
    this.dispatch = props.dispatch    
  }
  render() {   
    return (
      <BrowserRouter>
        <MainBody token={this.props.token} dispatch={this.dispatch}/>
      </BrowserRouter>
    );
  }
}
export default connect(mapStateToProps)(AppHandler);
