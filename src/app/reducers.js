import { combineReducers } from 'redux';
import {Reducer as AppReducer} from './base'
import {Reducer as CRMReducer, Reducer} from './crm' 
export default combineReducers({
    ...AppReducer,
    ...CRMReducer 
});