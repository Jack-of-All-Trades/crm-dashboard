const config = require('../config'),
      axios = require('axios')

export default axios.create({
  baseURL: config.api.url,
  timeout: config.api.timeout
});