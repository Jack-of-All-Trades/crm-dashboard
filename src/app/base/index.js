import React from 'react'
import { connect } from 'react-redux';
import Header from './components/Header';
import SideNav from './components/Sidenav';
import Footer from './components/Footer';
import PublicRoute from './components/Login/publicRoute'

import Login from './components/Login';
import {Route} from 'react-router-dom'
import styles from './styles/main-container.scss'

import Auth from './components/auth'


import settingsReducer from './reducers/settings';
import AuthReducer from './components/auth/reducer'
import UserReducer from './components/User/reducer'

export const Reducer = {
  "setting":settingsReducer,
	"Auth":AuthReducer,
	"user":UserReducer
}

const _ = require('lodash')
// need to change to class for components did mount
const App = ({children}) =>{	
	return (
		<Auth>
			<div className="main-app-container">	    
		    <section id="page-container" className="app-page-container">
		      <Header modules={children}/>
		      <div className="app-content-wrapper">
		        <div className={`container-fluid ${styles['main-app-container']}`}>
		          <div className="full-height">
		          	<PublicRoute exact path="/" component={Login} />
		            {React.Children.map(children, (child, i) => {
				          // Ignore the first child
				          return child
				        })}
		          </div>
		        </div>
		        <Footer />
		      </div>
		    </section>
		  </div>
		</Auth>
	)
}

export default App

export const NewApp = ({children}) =>{	
	return (
		<Auth>
			<div className="main-app-container">	    
		    <section id="page-container" className="app-page-container">
		      <Header modules={children}/>
		      <div className="app-content-wrapper">
		        <div className={`container-fluid ${styles['main-app-container']}`}>
		          <div className="full-height">
		          	<PublicRoute exact path="/" component={Login} />
		            {React.Children.map(children, (child, i) => {
				          // Ignore the first child
				          return child
				        })}
		          </div>
		        </div>
		        <Footer />
		      </div>
		    </section>
		  </div>
		</Auth>
	)
}