import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import Login from '../Login'
import PublicRoute from '../Login/publicRoute'
import { bindActionCreators } from 'redux';

const querystring = require('querystring');

import * as authActions from './actions'

import {
  BrowserRouter,
  Route,
  Redirect,
  Link
} from 'react-router-dom';


function mapStateToProps(state) {    
  return { 
    isAuth: state.Auth.get('isAuth'),
    token: state.Auth.get('token'),
    userProfile: state.Auth.get('userProfile')
  };
}

@connect(mapStateToProps)
class Auth extends React.Component {
  constructor(props) {
    super(props)
    this.authActions = bindActionCreators(authActions, props.dispatch)
  }
  componentDidMount() {
    var self = this
    if(!this.props.isAuth) {
      var cookieInfo = authActions.getToken() 
      if(!_.isEmpty(this.props.location.search)) {
        var query = this.props.location.search.replace('?',''),
            queryObj = querystring.parse(query)          
        if(!_.isEmpty(queryObj.token)) {
          this.authActions.verifyToken(queryObj.token, (token) => {
            authActions.saveToken(token)
          })
        } else {
          if(!_.isEmpty(cookieInfo)) {
            this.authActions.getUserFromToken(cookieInfo)
          }
        }
      } else {
        if(!_.isEmpty(cookieInfo)) {
          this.authActions.getUserFromToken(cookieInfo)
        }
      }
    } else {
      console.log('tehere')
    }
  }
  render() {
    var {isAuth, userProfile, token} = this.props    
    return this.props.children
  }
}

export default withRouter(Auth)