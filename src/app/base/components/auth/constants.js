export const UPDATETOKEN = "auth_update_token"
export const CHECKINGTOKEN = "auth_checking_token"
export const VERIFYTOKEN = "auth_verify_token"
export const LOGOUT = "auth_logout_user"
export const AUTHUSER = "auth_auth_user"
export const UPDATE_TOKEN = "auth_update_token"

export const COOKIE_NAME = 'crm'