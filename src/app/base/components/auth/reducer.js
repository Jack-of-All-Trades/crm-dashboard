const _ = require('lodash')

import {Map} from 'immutable'
import * as constant from './constants';


export default function Auth (state=Map({isAuth:false,token:"", userProfile: Map()}), action){

  let reducer = {}

  reducer[constant.REFRESHINGTOKEN] = ()=> {
    return state.set('isAuth', false).set('token',"").set('userProfile',Map())
  }

  reducer[constant.AUTHUSER] = ({token, userProfile}) => {
    return state.set('isAuth', true).set('token',token).set('userProfile',Map(userProfile))
  }

  reducer[constant.UPDATE_TOKEN] = ({token}) => {
    return state.set('token',token)
  }

  reducer[constant.LOGOUT] = () => {
    return state.set('isAuth', false).set('token',"").set('userProfile',Map())
  }

  reducer['default'] = ()=> {
    return state
  }
  
  return (reducer[action.type] || reducer['default'])(action)
};