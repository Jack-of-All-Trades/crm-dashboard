const jwtDecode = require('jwt-decode'),
      moment = require('moment')


import fetch from 'isomorphic-fetch'
import {AuthService} from '../services/request'
import * as constants from './constants'
import Cookies from 'universal-cookie';

const cookie = new Cookies()

export const getUserFromToken = (token) => {
  return function(dispatch) {
    dispatch({
      type: constants.CHECKINGTOKEN
    })
    var decoded = jwtDecode(token)    
    if(decoded.exp>0) {
      dispatch({
        type: constants.AUTHUSER,
        token: token,
        userProfile: decoded
      })      
    } else {
      decoded = null
    }    
    dispatch(refreshToken(token))
    return decoded
  }
}

export const getToken = () => {
  if(!_.isEmpty(cookie.get(constants.COOKIE_NAME)))
    return cookie.get(constants.COOKIE_NAME)
  return ''
}

export const saveToken = (token) => {  
  if(token == null) {
    return cookie.remove(constants.COOKIE_NAME)
  }
  var decoded = jwtDecode(token)
  var expired = (new Date().getTime()) + decoded.exp
  cookie.set(constants.COOKIE_NAME, token, {
    expires: new Date(expired),
    path: '/'
  })
}

export const verifyToken = (token, cb) => {
  return function(dispatch) {
    dispatch({
      type: constants.VERIFYTOKEN
    })
    var decoded = jwtDecode(token)
    if(decoded.exp< new Date().getTime()) {
      AuthService.post('/verify/'+token)
        .then(function(resp) {
          //decoded = jwtDecode(resp.data)
          dispatch({
            type: constants.AUTHUSER,
            token: token,
            userProfile: decoded
          })
          cb(token)
        })
    }
    return null
  }
}

export const refreshToken = (token)=> {  
  return function(dispatch) {
    dispatch({
      type: constants.CHECKINGTOKEN
    })    
    AuthService.post('/refresh/'+token)
      .then((resp)=>{        
        return resp.data
      })
      .then((token)=>{
        var decoded = jwtDecode(token)
        saveToken(token)
        dispatch({
          type: constants.AUTHUSER,
          token: token,
          userProfile: decoded
        })
      }).catch(function(err) {        
        saveToken(null)
      })
  }
     
}