import {Map,List} from 'immutable'
import * as constant from './constants'

const initialState = Map({
  users: Map(),
  allUsers: []
})

export default (state=initialState, action) => {  
  let reducer = {}

  reducer[constant.LOADING_USER] = ({id})=> {
    var users = state.get('users')
    if(!users.has(id)) {
      users = users.set(id, Map())
    }
    return state.set('users',users)
  }

  reducer[constant.LOADED_USER] = ({user})=> {
    var users = state.get('users')
    users = users.set(user.id, Map(user))
    return state
      .set('users', users)      
  }

  reducer[constant.LOADING_ALL_USERS] = () => {
    return state.set('allUsers',[])
  }

  reducer[constant.LOADED_ALL_USERS] = ({users}) => {
    return state.set('allUsers',users)
  }

  reducer['default'] = ()=> {
    return state
  }
  
  return (reducer[action.type] || reducer['default'])(action)
}