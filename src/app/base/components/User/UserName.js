import React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as UserAction from './action'

function mapStateToProps(state) {    
  return { 
    user: state.user.get('conversations')
  };
}

@connect(mapStateToProps)
export default class Communication extends React.Component {
  constructor(props) {
    super(props)    
    this.dispatch = this.props.dispatch
    this.fieldChange = this.fieldChange.bind(this)
    this.closeDialog = this.closeDialog.bind(this)
    this.saveCommunication = this.saveCommunication.bind(this)        
    this.communicationActions = bindActionCreators(CommunicationActions, props.dispatch)
    this.actions = [
      <FlatButton
        label="Cancel"
        primary={false}
        onTouchTap={this.closeDialog}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        onTouchTap={this.saveCommunication}
      />,
    ];
  }

  componentDidMount() {    
    if(!_.isEmpty(this.props.match.params.opportunityID)) {
      this.communicationActions.loadConversations('Opportunity',this.props.match.params.opportunityID)
    }
  }

  fieldChange(field,value) {    
    this.communicationActions.updateFormField(field,value)
  }

  closeDialog() {
    this.customerActions.toggleCustomerDialog(false)
    this.props.history.push('/crm')
  }

  saveCommunication() {
    this.communicationActions.saveRemark(this.props.formData)
  }

  render() {
    const {opportunity, conversations, customer, formData} = this.props    
    var title = customer.get('name')+"'s Communications"
    return (
      <Dialog
        title={title}
        actions={this.actions}
        model={true}
        autoScrollBodyContent={true}
        open={true}
        autoLockScrolling={true}
        onRequestClose={this.closeDialog} >
        <CommunicationForm formData={formData} handleValueChange={this.fieldChange} />
        <CommunicationList conversations={conversations} />
      </Dialog>
    )
  }
}