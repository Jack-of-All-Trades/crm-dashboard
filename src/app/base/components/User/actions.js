import * as constants from './constants'
import {APIService} from '../services/request'
import {Map, List} from 'immutable'

export function getUser(id) {
  if(_users.has(id)) {
    return new Promise((resolve, reject) => {
      resolve(_users.get(id))
    })
  }
  return fetch('/api/user/'+id)
    .then((data)=>{
      return data.json()
    })
    .then((user)=>{
      _users.set(id, user)
    })
}

export function getAllUser(mdouleName) {
  return (dispatch) => {
    dispatch({
      type: constants.LOADING_ALL_USERS
    });
    APIService.get("/user/",{params: {moduleName: mdouleName}})
      .then((resp)=> {
        dispatch({
          type: constants.LOADED_ALL_USERS,
          users: resp.data
        });
      })
  }
}

export function loadUser(id) {
  return (dispatch) => {
    dispatch({
      type: constants.LOADING_USER,
      id:id
    });
    APIService.get("/user/"+id)
      .then((resp)=> {
        dispatch({
          type: constants.LOADED_USER,
          user: resp.data
        });
      })
  }
}