import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'

import {
  BrowserRouter,
  Route,
  Redirect,
  Link
} from 'react-router-dom';


function mapStateToProps(state) {    
  return { 
    isAuth: state.Auth.get('isAuth')
  };
}

@connect(mapStateToProps)
class PrivateRoute extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const {component, isAuth, ...rest} = this.props
    var self = this    
    var Component = component
    const rendered = (props) => {      
      if(isAuth) {
        return (<Component {...props}/>)
      }
      return (
        <Redirect to={{
          pathname: '/',
          state: { from: props.location }
        }}/>
      )
    }
    return (<Route {...rest} render={props=>rendered(props)}/>)
  }
}

export default withRouter(PrivateRoute)