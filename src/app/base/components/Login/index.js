import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import QueueAnim from 'rc-queue-anim';
var config = require('../../config')


export default class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      brand: "crm"
    };
  }

  render() {
    return (
      <div className="page-login">
        <div className="main-body">
          <QueueAnim type="bottom" className="ui-animate">
            <div key="1">
              <div className="body-inner">
                <div className="card bg-white">
                  <div className="card-content">
                    <section className="logo text-center">
                      <h1><a href="#/">Login</a></h1>
                    </section>
                     <section className="text-center">
                      <RaisedButton href={config.auth.url+"/google-login?r="+config.auth.returnURL} label="Login Via Google" primary={true} />
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </QueueAnim>
        </div>
      </div>
    );
  }
}
