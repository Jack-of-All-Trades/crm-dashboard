import React from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { Link } from 'react-router';
import NavLeftList from './NavLeftList';
import NavRightList from './NavRightList';

@connect()
export default class Header extends React.Component {
  componentDidMount() {
    const sidebarToggler = this.sidebarBtn;
    const $sidebarToggler = $(sidebarToggler);
    const $body = $('#body');

    $sidebarToggler.on('click', (e) => {
      // _sidebar.scss, _page-container.scss
      $body.toggleClass('sidebar-mobile-open');
    });
  }

  render() {
    const {modules} = this.props
    return (
      <section className="app-header">
        <div
          className={classnames('app-header-inner', {
            'bg-color-light': true})}
                >
          <div className="hidden-lg-up float-left">
            <a href="javascript:;" className="md-button header-icon toggle-sidebar-btn" ref={(c) => { this.sidebarBtn = c; }}>
              <i className="material-icons">menu</i>
            </a>
          </div>

          <div className="brand hidden-md-down">
            <h2>CRM</h2>
          </div>

          <div className="d-flex align-content-center flex-wrap">
            <NavLeftList modules={modules} />
          </div>

          <div className="top-nav-right">
            
          </div>
        </div>
      </section>
    );
  }
}