import React from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
import { hashHistory } from 'react-router';
import Divider from 'material-ui/Divider';
import FlatButton from 'material-ui/FlatButton';
import {
  Link
} from 'react-router-dom';
const HeaderIconButtonStyle = {
  width: '60px',
  height: '60px'
};

const listItemStyle = {
  paddingLeft: '40px' // 36 + 16, algin with sub list
};

class NavLeftList extends React.Component {
  constructor(props) {
    super(props)
    this.getModulesNav = this.getModulesNav.bind(this)
  }
  getModulesNav() {
    return React.Children.map(this.props.modules,function(module, key) {      
      if(module.props.nav) {
        return module.props.nav()
      }
      else {
        return null
      }
    })
  }
  render() {
    return (
      <ul className="list-unstyled list-inline">
        <li key="dashboard" style={{paddingTop:"10px"}} className="list-inline-item">
          <FlatButton containerElement={<Link to="/crm" />} >
            <span className="nav-text">CRM</span>
          </FlatButton>
        </li>
        <li key="dashboard" style={{paddingTop:"10px"}} className="list-inline-item">
          <FlatButton containerElement={<Link to="/novaly/report" />} >
            <span className="nav-text">Amplifier</span>
          </FlatButton>
        </li>
        <li key="dashboard" style={{paddingTop:"10px"}} className="list-inline-item">
        <FlatButton containerElement={<Link to="/sf" />} >
          <span className="nav-text">Salon Finder</span>
        </FlatButton>
      </li>
      </ul>
    );
  }
}

module.exports = NavLeftList;
