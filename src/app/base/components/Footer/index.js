import React from 'react';
import { Link } from 'react-router';
var moment = require('moment')


const Footer = () => {
  var year = moment().format('Y')
  return (
    <section className="app-footer">
      <div className="container-fluid">
        <span className="float-left">
          <span>Copyright © <a className="brand" target="_blank" href="http://localhost">CRM</a> {year}</span>
        </span>
      </div>
    </section>
  )
}

module.exports = Footer;
