const config = require('../../config'),
      axios = require('axios')

export const AuthService = axios.create({
  baseURL: config.auth.url,
  timeout: config.auth.timeout
});

export const APIService = axios.create({
  baseURL: config.apiURL,
  timeout: config.auth.timeout
});