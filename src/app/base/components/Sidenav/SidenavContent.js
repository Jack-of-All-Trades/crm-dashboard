import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import FlatButton from 'material-ui/FlatButton';
import { Scrollbars } from 'react-custom-scrollbars';
import $ from 'jquery'


class SidebarContent extends React.Component {
  constructor(props) {
    super(props)
    this.modules = props.modules    
    this.getModulesNav = this.getModulesNav.bind(this)    
  }

  getModulesNav() {
    return this.props.modules.map(function(module, key) {      
      return module.moduleObj.Nav()
    })
  }

  componentDidMount() {
    const nav = this.nav;
    const $nav = $(nav);


    // Append icon to submenu
    // Append to child `div`
    $nav.find('.prepend-icon').children('div').prepend('<i class="material-icons">keyboard_arrow_right</i>');


    // AccordionNav
    const slideTime = 250;
    const $lists = $nav.find('ul').parent('li');
    $lists.append('<i class="material-icons icon-has-ul">arrow_drop_down</i>');
    const $As = $lists.children('a');

    // Disable A link that has ul
    $As.on('click', event => event.preventDefault());

    // Accordion nav
    $nav.on('click', (e) => {

      const target = e.target;
      const $parentLi = $(target).closest('li'); // closest, insead of parent, so it still works when click on i icons
      if (!$parentLi.length) return; // return if doesn't click on li
      const $subUl = $parentLi.children('ul');


      // let depth = $subUl.parents().length; // but some li has no sub ul, so...
      const depth = $parentLi.parents().length + 1;

      // filter out all elements (except target) at current depth or greater
      const allAtDepth = $nav.find('ul').filter(function () {
        if ($(this).parents().length >= depth && this !== $subUl.get(0)) {
          return true;
        }
        return false;
      });
      allAtDepth.slideUp(slideTime).closest('li').removeClass('open');

      // Toggle target
      if ($parentLi.has('ul').length) {
        $parentLi.toggleClass('open');
      }
      $subUl.stop().slideToggle(slideTime);

    });


    // HighlightActiveItems
    const $links = $nav.find('a');
    const currentLocation = ""
    function highlightActive(pathname) {
      const path = `#${pathname}`;

      $links.each((i, link) => {
        const $link = $(link);
        const $li = $link.parent('li');
        const href = $link.attr('href');
        // console.log(href);

        if ($li.hasClass('active')) {
          $li.removeClass('active');
        }
        if (path.indexOf(href) === 0) {
          $li.addClass('active');
        }
      });
    }
  }

  render() {
    return (
      <Scrollbars
        universal
        autoHeight
        autoHeightMin={100}
        autoHeightMax={200}>
        <ul className="nav">
          <li className="nav-header"><span>Menu</span></li>
          <li>
            <FlatButton href="#" containerElement={<Link to="/" />}>
              <i className="nav-icon material-icons">dashboard</i>
              <span className="nav-text">Dashboard</span>
            </FlatButton>
          </li> 
          {this.getModulesNav()} 
        </ul>
      </Scrollbars>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  ...state,
  modules: ownProps.modules
});


export default connect(mapStateToProps)(SidebarContent);