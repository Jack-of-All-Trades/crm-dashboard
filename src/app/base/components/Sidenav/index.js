import React from 'react';
import { connect } from 'react-redux';
import {
  BrowserRouter,
  Route,
  Link
} from 'react-router-dom'
import SidenavContent from './SidenavContent';
import $ from 'jquery'

class Sidebar extends React.Component {

  constructor(props) {
    super(props)     
    this.dispatch = props.dispatch
    this.moduleObjs = []
  }

  componentDidMount() {
    const $body = $('#body');     
  }

  onToggleCollapsedNav(e) {
    const val = false;
    const { handleToggleCollapsedNav } = this.props;
    handleToggleCollapsedNav(val);
  }

  render() {
    let toggleIcon = null;
    if (1==2) {
      toggleIcon = <i className="material-icons">radio_button_unchecked</i>;
    } else {
      toggleIcon = <i className="material-icons">radio_button_checked</i>;
    }
    return (
      <nav className='app-sidebar bg-color-dark'>
        <section className='sidebar-header bg-color-primary'>          
          <Link to="/" className="brand">CRM</Link>
          <a href="javascript:;" className="collapsednav-toggler">
            {toggleIcon}
          </a>
        </section>

        <section className="sidebar-content">
          <SidenavContent modules={this.props.modules} />
        </section>

        <section className="sidebar-footer">
          <ul className="nav">
            <li>
              <a target="_blank" href="/">
                <i className="nav-icon material-icons">help</i>
                <span className="nav-text"><span>Help</span> & <span>Support</span></span>
              </a>
            </li>
          </ul>
        </section>
      </nav>
    );
  }
}

const mapStateToProps = (state,ownProps) => ({
  modulesLoaded:state.Modules.loadedModule || false,
  modules: ownProps.modules
});

var SideNav = connect(mapStateToProps)(Sidebar);

export default SideNav