module.exports = {
  apiURL: BASE_API_URL,
  auth: {
    url: BASE_AUTH_URL,
    returnURL: BASE_AUTH_RETURN_URL,
    timeout: BASE_AUTH_TIMEOUT
  }
}
