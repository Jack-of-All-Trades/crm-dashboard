import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import settings from './settings';
import user from '../components/User/reducer'
import Auth from '../components/auth/reducer'

const reducers = {
	settings,
  Auth
};

export default function createReducer(asyncReducers) {
  return combineReducers({
    settings,
    Auth,
    ...asyncReducers
  });
}