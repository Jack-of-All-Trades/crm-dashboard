import { Provider } from 'react-redux'
import React from 'react'
import {render} from 'react-dom'
import { AppContainer } from 'react-hot-loader';
import Apphandler from './app'
import injectTapEventPlugin from 'react-tap-event-plugin';
import reducers from './app/reducers'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

try {
  injectTapEventPlugin(); // Needed for onTouchTap for Material UI
} catch(e) {
  console.log(e)
}

const store = createStore(reducers, {}, applyMiddleware(thunk));

render(
  <AppContainer>
  	<Provider store={store}>
      <Apphandler/>
  	</Provider>
  </AppContainer>,
  document.getElementById('app-container')
);

if (module.hot) {
  module.hot.accept('./app', () => {
  	require('./app')
    render(
      <AppContainer>
  	  	<Provider store={store}>
          <Apphandler/>
  	    </Provider>	    
      </AppContainer>,
      document.getElementById('app-container')
    );
  });
}