FROM keymetrics/pm2-docker-alpine:8

WORKDIR /app
COPY ./ /app/

RUN pm2 install pm2-auto-pull
RUN npm install && npm run build

ENV NODE_ENV=production

EXPOSE 1337

CMD ["pm2-docker", "--json", "./.build/main.js"]
